import React from "react";

import Intro from "../components/Intro";
import LatestPost from "../components/LatestPost";

import {
  Row,
  Col
} from "shards-react";
import _ from "lodash";
import moment from "moment";
import { observer } from "mobx-react";
import UserStore from "../../MobxStore/UserStore";
import PopularArticles from "../components/Portfolio/PopularArticles";
import PopularCategories from "../components/Portfolio/PopularCategories";
import SocialNetwork from "../components/SocialNetwork";
import Footer from "../components/Footer";
import YoutubeEmbed from "../components/YoutubeEmbed";
import {
	withRouter
} from 'react-router-dom';

@observer
class Home extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
    }
  }

  isEven = (v) => { 
    return v !== "justtest"; 
  }

  componentDidMount = async () =>{
    UserStore.setPostPage(1, 4);
    const postResponse  = await UserStore.getAllPost(false);
    if(postResponse){
      await UserStore.getFullCategories();
      return;
    }else{
        alert("Lost internet connection")
    }
  }
  render() {
    const postList = UserStore.postList.articles;
    return (
      <div className="home-page">
        <Intro></Intro>
        <div className="latest-product pl-5 pr-5 pt-5 pb-3">
          <Row>
            <Col lg="8" md="8" sm="12">
              <h2 className="text-center mb-4 latest-title">Latest Blog</h2>
              {UserStore.postList && postList
              ? (
                <Row>
                  {postList && postList.map((list, idx) => (
                    <LatestPost
                      backgroundImage={UserStore.BASE_PHOTO_API + list.avatar}
                      categoryTheme="bright"
                      category={_.filter(list.articleCategories, this.isEven)}
                      author={list.authorName}
                      authorAvatar={UserStore.BASE_PHOTO_API + list.authorAvatar}
                      title={list.title}
                      description={list.shortDescription}
                      date={moment(new Date(list.createdAt)).format("DD MMMM YYYY")}
                      postUrl={"http://localhost:9000/bai-viet/" + list.slug}
                    />
                  ))}
                </Row>
              )
              : null
            }
            </Col>
            <Col lg="4" md="4" sm="12">
            <h2 className="text-center mb-4 latest-title">Latest Video</h2>
              <YoutubeEmbed embedId="YhQQ8VwvR6A"/>
            </Col>
          </Row>
        </div>
        <div className="our-desination">
          <div class="wrapper pb-4">
              <div class="triangle-down">
                <div></div>
              </div>
          </div>  
          <h2 className="text-center pb-5 destination-title pt-5">Những Nơi Đã Khám Phá</h2>
          <div className="map-container">
            <Row>
              <Col lg="4" md="4" sm="12" className="text-center">
                <img
                  className="m-auto"
                  src={require("../assets/photo/dalat-destination.png")}
                  alt="Our Destination"
                />
                <h3 className="mt-3 text-center">Đà Lạt</h3>
              </Col>
              <Col lg="4" md="4" sm="12" className="text-center">
                <img
                  className="m-auto"
                  src={require("../assets/photo/ninhthuan-destination.png")}
                  alt="Our Destination"
                />
                <h3 className="mt-3 text-center">Ninh Thuận</h3>
              </Col>
              <Col lg="4" md="4" sm="12" className="text-center">
                <img
                  className="m-auto"
                  src={require("../assets/photo/tayninh-destination.png")}
                  alt="Our Destination"
                />
                <h3 className="mt-3 text-center">Tây Ninh</h3>
              </Col>
            </Row>
          </div>
          <div>
            <div>
              <section>
                <div className="wave water"></div>
                <div className="wave water"></div>
                <div className="wave water"></div>
                <div className="wave water"></div>                  
              </section>
            </div>
          </div>
        </div>
        <div className="popular-post pt-4">
          <h2 className="text-center pb-4 latest-title">Popular Post</h2>
          <Row className="popular-post-contain ml-auto mr-auto">
            <PopularArticles
              backgroundImage={require("../assets/photo/1.jpg")}
              title="How To Make"
              smallTitle="A Blog"
            />
            <PopularArticles
              backgroundImage={require("../assets/photo/1.jpg")}
              title="How To Make"
              smallTitle="A Blog"
            />
            <PopularArticles
              backgroundImage={require("../assets/photo/1.jpg")}
              title="How To Make"
              smallTitle="A Blog"
            />
            <PopularArticles
              backgroundImage={require("../assets/photo/1.jpg")}
              title="How To Make"
              smallTitle="A Blog"
            />
          </Row>
          <Row className="popular-categories-contain ml-auto mr-auto mt-2">
            <PopularCategories
              backgroundImage={require("../assets/photo/1.jpg")}
              title="About Us"
              url="https://www.google.com/"
              alt="test"
            />
            <PopularCategories
              backgroundImage={require("../assets/photo/1.jpg")}
              title="Camera"
              url="https://www.google.com/"
              alt="test"
            />
            <PopularCategories
              backgroundImage={require("../assets/photo/1.jpg")}
              title="Du Lịch Có Phí"
              url="https://www.google.com/"
              alt="tesst"
            />
            <PopularCategories
              backgroundImage={require("../assets/photo/1.jpg")}
              title="Du Lịch Miễn Phí"
              url="https://www.google.com/"
              alt="test"
            />
            <PopularCategories
              backgroundImage={require("../assets/photo/1.jpg")}
              title="Homestay"
              url="https://www.google.com/"
              alt="test"
            />
            <PopularCategories
              backgroundImage={require("../assets/photo/1.jpg")}
              title="Phim Ngắn"
              url="https://www.google.com/"
              alt="test"
            />
          </Row>
          <hr className="mt-4 mb-3 separate-line"/>
          <Row className="social-network-contain ml-auto mr-auto mt-4">
            <h4 className="mr-0 ml-0 pr-0 pl-0 social-title">THEO DÕI TỤI MÌNH:</h4>
            <SocialNetwork
              photo={require("../assets/photo/facebook.png")}
              alt="test"
              url="https://www.facebook.com/moichanmondep"
            />
            <SocialNetwork
              photo={require("../assets/photo/instagram.png")}
              alt="test"
              url="https://www.instagram.com/moichanmondep/"
            />
            <SocialNetwork
              photo={require("../assets/photo/tik-tok.png")}
              alt="test"
              url="https://www.tiktok.com/@moichanmondep"
            />
            <SocialNetwork
              photo={require("../assets/photo/youtube.png")}
              alt="test"
              url="https://www.youtube.com/channel/UCp5Dg5p4-iCl9L7yV85zZKw"
            />
          </Row>
        </div>
        <Footer/>
      </div>
    )
  }
}
export default withRouter(Home);
