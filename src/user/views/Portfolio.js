import React from "react";
import PortfolioDescription from "../components/Portfolio/PortfolioDescription";
import {
    Row,
    Col
} from "shards-react";
import WorkingSkills from "../components/Portfolio/WorkingSkills";
import TeamMembers from "../components/Portfolio/TeamMembers";
import Gallery from "react-photo-gallery";
import Footer from "../components/Footer";
import YoutubeEmbed from "../components/YoutubeEmbed";
import '../assets/portfolio/style.css';
import PageIntro from "../components/PageIntro";

class Portfolio extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
    }
  }
  render() {
    const photos = [
      {
        src: "https://source.unsplash.com/2ShvY8Lf6l0/800x599",
        width: 4,
        height: 3
      },
      {
        src: "https://source.unsplash.com/Dm-qxdynoEc/800x799",
        width: 1,
        height: 1
      },
      {
        src: "https://source.unsplash.com/qDkso9nvCg0/600x799",
        width: 3,
        height: 4
      },
      {
        src: "https://source.unsplash.com/iecJiKe_RNg/600x799",
        width: 3,
        height: 4
      },
      {
        src: "https://source.unsplash.com/epcsn8Ed8kY/600x799",
        width: 3,
        height: 4
      },
      {
        src: "https://source.unsplash.com/NQSWvyVRIJk/800x599",
        width: 4,
        height: 3
      }
    ];
    return (
      <div className="portfolio-page">
        <PageIntro
            first="Our"
            second="Portfolio"
            backgroundImage={require("../assets/photo/my-camera.jpg")}
        />
        <Row className="working-skill-container">
          <Col lg="12" md="12" sm="12">
            <PortfolioDescription
              title="3 NĂM KINH NGHIỆM"
              description="Ngoài Việc làm Travel Vlog. Tụi mình còn có kinh nghiệm trong các lĩnh vực làm video review, quảng bá cho các nhà hàng, resort, khách sạn, khu du lịch,... . 
              Đồng thời cũng làm các bộ phim ngắn mang giá trị nhân văn cao."
            />
          </Col>
          <Col lg="12" md="12" sm="12">
            <WorkingSkills/>
          </Col>
        </Row>
        <div className="team-members-container">
          <h2 className="text-center pb-4 team-title pt-5">Thành Viên Chính</h2>
          <Row>
            <TeamMembers
              avatar={require("../assets/photo/quoc.jpg")}
              name="Kiến Quốc"
              position="Team Leader"
            />
            <TeamMembers
              avatar={require("../assets/photo/xuan.jpg")}
              name="Aileen Pham"
              position="Content Creator"
            />
            <TeamMembers
              avatar={require("../assets/photo/khanh.jpg")}
              name="Lâm Khánh"
              position="Cameraman"
            />
          </Row>
        </div>
        <div className="portfolio-video-container mt-3">
          <h2 className="text-center pb-4 team-title pt-4">Một Số Video</h2>
          <Row>
            <Col lg="6" md="6" sm="12" className="mb-4">
              <YoutubeEmbed embedId="bdaxM1nRXJ4"/>
            </Col>
            <Col lg="6" md="6" sm="12" className="mb-4">
              <YoutubeEmbed embedId="YhQQ8VwvR6A"/>
            </Col>
            <Col lg="6" md="6" sm="12" className="mb-4">
              <YoutubeEmbed embedId="UEJtxsnY2T0"/>
            </Col>
            <Col lg="6" md="6" sm="12" className="mb-4">
              <YoutubeEmbed embedId="lSSA6Mbwnb8"/>
            </Col>
          </Row>
        </div>
        <h2 className="text-center pb-4 team-title pt-5">Một Số Hình Ảnh</h2>
        <div className="gallery">
          <div className="my-overlay"></div>
          <Gallery photos={photos} direction={"column"}/>
        </div>
        <Footer/>
      </div>
    )
  }
}
export default Portfolio;
