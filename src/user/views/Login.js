import React from "react";
import PageIntro from "../components/PageIntro";
import {
  Card,
  CardHeader,
  CardBody,
  Form,
  FormInput,
  FormGroup,
  FormFeedback,
  Row,
  Col,
  Button
} from "shards-react";
import '../assets/login/style.css';
import UserStore from "../../MobxStore/UserStore";
import Footer from "../components/Footer";

class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      username:'',
      password:'',
      unvalid: false,
      interneterror: false,
      data: []
    }
  }

  handleUsertname(text){
    this.setState({username: text.target.value})
  }
  handlePassword(text){
    this.setState({password: text.target.value})
  }
  handleSubmit = async (event) =>{
    event.preventDefault();
    const user = {
        username: this.state.username,
        password: this.state.password
    };
    const loginResponse = await UserStore.login(user)
    if(loginResponse.status == 401){
      this.setState({unvalid: true})
      this.setState({interneterror: false})
    }else if(loginResponse.status == 200){
      const user = await UserStore.logedin();
      if(user.data.role == "admin"){
        this.props.history.push('/add-new-post');
      }else{
        this.props.history.push('/home');
      }
      alert("login success")
    }else{
      alert("Internet Error. Please check your connection")
    }
  }
  render() {
    return (
      <div className="login-page">
        <PageIntro
          first="Đăng Nhập"
          second="Tài Khoản"
          backgroundImage={require("../assets/photo/blog-intro.jpg")}
        />
        <Card className="p-3 h-100 login-form">
          <CardHeader className="login-form-title">
            <h2>Đăng Nhập Tài Khoản</h2>
            { this.state.unvalid 
              ? ( 
                <h5>Sai Tài Khoản Hoặc Mật khẩu</h5>
              )
              : null
            }
            { this.state.interneterror 
              ? ( 
                <h5>Lỗi Kết Nối Mạng</h5>
              )
              : null
            }
          </CardHeader>
          <CardBody>
            <Form onSubmit={this.handleSubmit}>
              <a className="text-muted d-block mb-2">Tên Tài Khoản: </a>
              <FormGroup>
                  <FormInput
                  placeholder="Hãy nhập tên tài khoản của bạn..."
                  //value=""
                  onChange={(text) => {this.handleUsertname(text)}}
                  />
              </FormGroup>
              <a className="text-muted d-block mb-2">Mật Khẩu: </a>
              <FormGroup>
                  <FormInput
                  type="password"
                  placeholder="Hãy nhập mật khẩu của bạn..."
                  //value="myCoolPassword"
                  onChange={(text) => {this.handlePassword(text)}}
                  />
              </FormGroup>
              <Button className="mb-2 mr-1" type="submit">Đăng Nhập</Button>
              <br/>
              <div className="d-inline-flex mt-2">
                <a className="text-muted d-block mb-2 forget mr-2" href="#">Quên Mật Khẩu?</a>
                <p className="text-muted d-block mb-2">Chưa có tài khoản? <a href="/dang-ky">Đăng Ký</a></p>
              </div>
            </Form>
            <Row className="login-with-social mt-3">
              <Col lg="6" md="12" sm="12">
                <button className="login-social-btn"><i class="fab fa-facebook-f"></i> Đăng Nhập Bằng Facebook</button>
              </Col>
              <Col lg="6" md="12" sm="12">
                <button className="login-social-btn"><i class="fas fa-envelope"></i> Đăng Nhập Bằng Gmail</button>
              </Col>
            </Row>
          </CardBody>
        </Card>
        <Footer/>
      </div>  
    )
  }
}
export default Login;
