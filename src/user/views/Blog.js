import React from "react";
import Footer from "../components/Footer";
import Slideshow from "../components/Blog/Slideshow";
import DropdownNavigation from "../components/DropdownNavigation";
import {
  Row,
  Col,
  Button
} from "shards-react";
import BlogPopularPost from "../components/Blog/BlogPopularPost";
import BlogArticles from "../components/Blog/BlogArticles";
import '../assets/blog/style.css';
import PageIntro from "../components/PageIntro";
import UserStore from "../../MobxStore/UserStore";
import PaginationComponent from "react-reactstrap-pagination";
import _ from "lodash";
import { observer } from "mobx-react";
import moment from "moment";
import SearchArticles from "../components/Blog/SearchArticles";
import SelectCategories from "../components/Blog/SelectCategories";
import MainPopup from "../components/MainPopup";

@observer
class Blog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedPage: 1,
      pageTotal: 1,
      fullList: []
    }
    this.handleSelected = this.handleSelected.bind(this);
    this.searchingPost = this.searchingPost.bind(this);
  }

  isEven = (v) => { 
    return v !== "justtest"; 
  }

  handleSelected = async (selectedPage) => {
    this.setState({ selectedPage: selectedPage });
    UserStore.setPostPage(selectedPage, 6);
    const postResponse  = await UserStore.getAllPost(false);
    if(postResponse){
        return;
    }else{
        alert("Lost internet connection")
    }
  }

  searchingPost = async () =>{
    console.log("searching...")
    await UserStore.getAllPost(true);
  }

  componentDidMount = async () =>{
    UserStore.setPostPage(1, 6);
    const postResponse  = await UserStore.getAllPost(false);
    console.log(UserStore.postList)
    this.setState({fullList: postResponse})
    await UserStore.getLikePost(1, 2);
    if(postResponse){
      await UserStore.getFullCategories();
      return;
    }else{
        alert("Lost internet connection")
    }
  }
  render() {
    const postList = UserStore.postList.articles;
    const highLikePost = UserStore.highLikePost.articles;
    const postTotal = UserStore.postList.total;
    return (
      <div className="blog-page">
        <MainPopup/>
        <PageIntro
          first="Bài Viết"
          second="Mới Nhất"
          backgroundImage={require("../assets/photo/blog-intro.jpg")}
        />
        <Row className="favorite-blog">
          <Col lg="6" md="6" sm="12" className="mb-4">
          {UserStore.postList && postList
            ? (
            <Slideshow
              input={postList}
              length={postTotal.total}
              ratio={`16:9`}
            />
            )
            : null
          }
          </Col>
          <Col lg="6" md="6" sm="12" className="mb-4">
          {UserStore.highLikePost && highLikePost
            ? (
              <div>
                {highLikePost && highLikePost.map((list, idx) => (
                <BlogPopularPost
                  backgroundImage={UserStore.BASE_PHOTO_API + list.avatar}
                  categoryTheme="dark"
                  category={_.filter(list.articleCategories, this.isEven)}
                  author={list.authorName}
                  authorAvatar={UserStore.BASE_PHOTO_API + list.authorAvatar}
                  title={list.title}
                  date={moment(new Date(list.createdAt)).format("DD MMMM YYYY")}
                  postContent={list.shortDescription}
                  postUrl={"http://localhost:9000/bai-viet/" + list.slug}
                />
                ))}
              </div>
            )
            : null
          }
          </Col>
        </Row>
        <h2 className="text-center pb-5 team-title pt-3">Bài Viết</h2>
        <Row className="mb-5 blog-search-container">
          <Col lg="6" md="12" sm="12" className="mb-3">
            <SearchArticles
              data={this.state.fullList.articles}
            />
          </Col>
          <Col lg="3" md="6" sm="12">
            <SelectCategories
              items={UserStore.categoriesFullList}
            />
          </Col>
          <Col lg="3" md="6" sm="12">
            <Button primary className="w-100" onClick={this.searchingPost}>Tìm Kiếm</Button>
          </Col>
        </Row>
        {UserStore.postList && postList
          ? (
            <Row className="blog-articles-container">
              {postList && postList.map((list, idx) => (
                <BlogArticles
                  key={idx}
                  backgroundImage={UserStore.BASE_PHOTO_API + list.avatar}
                  category={_.filter(list.articleCategories, this.isEven)}
                  author={list.authorName}
                  authorAvatar={list.authorAvatar}
                  title={list.title}
                  date={moment(new Date(list.createdAt)).format("DD MMMM YYYY")}
                  postContent={list.shortDescription}
                  postUrl={"http://localhost:9000/bai-viet/" + list.slug}
                />
              ))}
            </Row>
          )
          : null
        }
        <PaginationComponent
          className="pagination"
          totalItems={postTotal && postTotal.total ? postTotal.total : 0}
          pageSize={UserStore.postLimit}
          onSelect={this.handleSelected}
          defaultActivePage={1}
        />     
        <Footer/>
      </div>
    )
  }
}
export default Blog;
