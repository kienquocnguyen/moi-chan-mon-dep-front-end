import React from "react";
import Footer from "../components/Footer";
import {
  Row,
  Col
} from "shards-react";
import BlogArticles from "../components/Blog/BlogArticles";
import '../assets/blog/style.css';
import PageIntro from "../components/PageIntro";
import UserStore from "../../MobxStore/UserStore";
import PaginationComponent from "react-reactstrap-pagination";
import _ from "lodash";
import { observer } from "mobx-react";
import moment from "moment";

@observer
class BlogByCategory extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedPage: 1,
      pageTotal: 1,
      postList: []
    }
    this.handleSelected = this.handleSelected.bind(this);
  }

  isEven = (v) => { 
    return v !== "justtest"; 
  }
  handleSelected = async (selectedPage) => {
    this.setState({ selectedPage: selectedPage });
    UserStore.setPostPage(selectedPage, 6);
    const postResponse  = await UserStore.getAllPost(false);
    if(postResponse){
        return;
    }else{
        alert("Lost internet connection")
    }
  }
  componentDidMount = async () =>{
    await UserStore.getCategoryBySlug(this.props.match.params.slug)
    UserStore.setPostPage(1, 6);
    const postResponse  = await UserStore.getAllPost(true);
    if(postResponse){
      return;
    }else{
      alert("Lost internet connection")
    }
  }
  render() {
    const postList = UserStore.postList.articles;
    const postTotal = UserStore.postList.total;
    return (
      <div className="blog-page">
        {UserStore.postCategory[0]
        ? (
            <PageIntro
                first="Bài Viết"
                second={UserStore.postCategory[0].title}
                backgroundImage={UserStore.BASE_PHOTO_API + UserStore.postCategory[0].avatar}
            />
        ) : (
            <PageIntro
                first="Bài Viết"
                second="Mới Nhất"
                backgroundImage={require("../assets/photo/blog-intro.jpg")}
            />
        )}
        {UserStore.postCategory[0]
        ? (
          <div>
            <h2 className="text-center pb-3 team-title pt-3">{UserStore.postCategory[0].title}</h2>
            <p className="category-description mb-5 mt-3">{UserStore.postCategory[0].description}</p>
          </div>
        ) : (
          <h2 className="text-center pb-3 team-title pt-3">Bài Viết</h2>
        )}
        {UserStore.postList && postList
            ? (
              <Row className="blog-articles-container">
                {postList && postList.map((list, idx) => (
                  <BlogArticles
                    key={idx}
                    backgroundImage={UserStore.BASE_PHOTO_API + list.avatar}
                    category={_.filter(list.articleCategories, this.isEven)}
                    author={list.authorName}
                    authorAvatar={list.authorAvatar}
                    title={list.title}
                    date={moment(new Date(list.createdAt)).format("DD MMMM YYYY")}
                    postContent={list.shortDescription}
                    postUrl={"http://localhost:9000/bai-viet/" + list.slug}
                  />
                ))}
              </Row>
            )
            : null
        }
        <PaginationComponent
            className="pagination"
            totalItems={postTotal && postTotal.total ? postTotal.total : 0}
            pageSize={UserStore.postLimit}
            onSelect={this.handleSelected}
            defaultActivePage={1}
        />     
       <div className="d-block m-auto">
          
        </div>
        <Footer/>
      </div>
    )
  }
}
export default BlogByCategory;
