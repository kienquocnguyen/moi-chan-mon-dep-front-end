import React from "react";
import '../assets/single-post/style.css';
import SinglePostIntro from "../components/SinglePost/SinglePostIntro";
import {
  Row,
  Col,
  Button,
  Form
} from "shards-react";
import ReactQuill, { Quill } from "react-quill";
import quillEmoji from 'quill-emoji';
import PaginationComponent from "react-reactstrap-pagination";
import "react-quill/dist/quill.snow.css";
import "quill-emoji/dist/quill-emoji.css";
import MainPopup from "../components/MainPopup.js"
import Sidebar from "../components/SinglePost/Sidebar";
import ReactHtmlParser from 'react-html-parser';
import UserStore from '../../MobxStore/UserStore';
import _ from "lodash";
import { observer } from "mobx-react";

let commentPageLimit = 3;
@observer
class SinglePost extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      content: "<h1>hahfahfdhfdshsfd</h1><br/><p>asfdgasgasdgasgagsaegadsgsadgasgaegeagesagae</p>",
      checkLiked: false,
      postCategoriesUpdated: "",
      commentContent: "",
      updatedComment: "",
      updating: null,
      updatingArray: [],
      isItemContentVisible: {},
      selectedCommentPage: 1,
      pageCommentTotal: 1
    }
    this.totalLike = 0;
  }

  handleLike = async () =>{
    const user = await UserStore.logedin();
    if(user.status == 200){
      if(UserStore.likePostChecked == true){
        this.totalLike = this.totalLike - 1;
        await UserStore.unLikePost(UserStore.singlePost._id, UserStore.userInfo._id);
        await UserStore.checkLikedPost(UserStore.singlePost._id);
        this.setState({checkLiked: UserStore.likePostChecked})
      }else{
          const body = {
            articleId: UserStore.singlePost._id,
            articleTitle: UserStore.singlePost.title
          }
          this.totalLike = this.totalLike + 1;
          await UserStore.likePost(body);
          await UserStore.checkLikedPost(UserStore.singlePost._id);
          this.setState({checkLiked: UserStore.likePostChecked})
      }
    }else{
      alert("You have to login before do this function");
    }
  }

  componentDidMount = async () => {
    await UserStore.logedin();
    await UserStore.getLikePost(1, 3);
    const getSinglePost = await UserStore.getSinglePost(this.props.match.params.slug);
    if(getSinglePost[0] == null){
      return;
    }
    await UserStore.checkLikedPost(UserStore.singlePost._id);
    this.setState({checkLiked: UserStore.likePostChecked})
    if(UserStore.singlePost.totalLike){
      this.totalLike = UserStore.singlePost.totalLike;
    }else{
      this.totalLike = 0;
    }
    let getComment = await UserStore.getPostComments(UserStore.singlePost._id);
    if(getComment){
      const updatingArray = [];
      UserStore.postComments.comments.map((comments, i) => {
        updatingArray.push(comments[i]);
      });
      this.setState({
        updatingArray
      });
    }else{
      getComment = "";
    }
    function isEven(v) { return v !== "justtest"; }
    this.setState({postCategoriesUpdated: _.filter(UserStore.singlePost.articleCategories, isEven)})
  }

  handleCommentChange = (html) =>{
    this.setState({commentContent: html});
  }

  sendComment = async () =>{
    //Submit the body comment
    const htmlContainer = document.createElement('div');
    htmlContainer.innerHTML = this.state.commentContent;
    const body = {
      postId: UserStore.singlePost._id,
      postTitle: UserStore.singlePost.title,
      type: "comment",
      content: htmlContainer.innerHTML
    }
    const createComment = await UserStore.createComment(body);
    if(createComment.status == 401){
      UserStore.setPopup(true, "Unauthorized");
      UserStore.setPopupMessages("Bạn cần đăng nhập để thực hiện tính năng này.");
    }
    this.setState({commentContent: ""});
    await UserStore.getPostComments(UserStore.singlePost._id);
  }

  handleSelected = async (selectedCommentPage) => {
    this.setState({ selectedCommentPage: selectedCommentPage });
    UserStore.setPostCommentPage(selectedCommentPage, 4);
    const postCommentResponse  = await UserStore.getPostComments(UserStore.singlePost._id);
    if(postCommentResponse){
        return;
    }else{
        alert("Lost internet connection")
    }
  }
  editComment = async (comment, idx) => {
    const user = await UserStore.logedin();
    if(user.status == 401){
      UserStore.setPopup(true, "Unauthorized")
      UserStore.setPopupMessages("Bạn cần đăng nhập để thực hiện tính năng này.");
      this.props.history.push('/home');
    }else{
      this.setState({updatedComment: comment.content})
      this.setState({
        updating: idx
      });
    }
  }
  stopEditComment(comment, idx) {
    this.setState({
      updating: null
    });
  }
  upadteCommentChange = (html) =>{
    this.setState({updatedComment: html})
  }
  sendEditedComment = async (comment) =>{
    comment.content = this.state.updatedComment
    await UserStore.editComment(comment);
    await UserStore.getPostComments(UserStore.singlePost._id);
    this.setState({
      updating: null
    });
  }
  detleteComment = async (comment) =>{
    UserStore.setDeleteCommentId(comment._id)
    UserStore.setPopup(true, "deleteComment")
  }
  render() {
    const user = UserStore.userInfo;
    const postCommentTotal = UserStore.postComments.total;
    return (
      <div className="single-post-page mt-1">
        {UserStore.showPopup
          ? (
            <MainPopup
              text='Close Me'
            />
          ) : null
        } 
        {UserStore.singlePost
          ? (
            <SinglePostIntro
              backgroundImage={"http://localhost:3000/uploads/" + UserStore.singlePost.avatar}
              category={this.state.postCategoriesUpdated}
              author={UserStore.singlePost.authorName}
              commentsAmount={postCommentTotal && postCommentTotal.total ? postCommentTotal.total : 0}
              title={UserStore.singlePost.title}
            />
          ):
          (
            <SinglePostIntro
              backgroundImage={require("../assets/photo/notfound_photo.jpg")}
              title="Xin Lỗi Tụi Mình Không tìm thấy trang này"
            />
          )
        }
        <Row className="sigle-post-description mt-5 pt-3">
            <Col lg="8" md="8" sm="12" className="mb-5">
            {UserStore.singlePost
            ? (
              <div className="single-post-content">
                {ReactHtmlParser(UserStore.singlePost.description)}
              </div>
            ):
            (
              <div className="single-post-content">
                <h2>Trang Này Đã Bị Xóa Hoặc Không Tồn Tại</h2>
              </div>
            )}
              {/* Like And Comment */}
              <div className="like-container">
                  <Button className="mr-1 like-button" onClick={this.handleLike}>
                      <i className={this.state.checkLiked ? "material-icons like-icon": "material-icons"} >{this.state.checkLiked ? "favorite": "favorite_border"}</i>
                  </Button>
                  <span>{this.totalLike} người thích bài viết.</span>
              </div>
              <div className="comment-input mb-4">
                <Form className="mb-4 d-inline-flex w-100">
                  <div className="active-user-info">
                    <img
                      className="user-comment-avatar rounded-circle mr-2"
                      src={user && user.avatar ? UserStore.BASE_PHOTO_API + user.avatar : require("../../admin/images/avatars/0.jpg")}
                      alt="User Avatar"
                    />
                    <span>{user.firstName} {user.lastName}</span>
                  </div>
                  <ReactQuill
                    id="rich-text-comment"
                    ref={(el) => this.quill = el}
                    className="rich-text-comment"
                    onChange={this.handleCommentChange}
                    placeholder="Type your comment..."
                    value={this.state.commentContent}
                    contenteditable="true"
                    modules={{
                      toolbar: {
                        container: [
                          ['emoji']
                        ]
                      },
                      clipboard: {
                        // toggle to add extra line breaks when pasting HTML:
                        matchVisual: false,
                      },
                      'emoji-toolbar': true,
                      "emoji-textarea": true,
                      "emoji-shortname": true
                    }}
                    formats={SinglePost.formats}
                  >
                  </ReactQuill>
                </Form>
                <div className="send-container">
                  <button type="button" className="invisible mx-auto p-0" onClick={this.sendComment}>
                    <h4 className="material-icons p-0 mb-0 visible">send</h4>
                  </button>
                </div>
              </div>
              {UserStore.postComments && UserStore.postComments.comments
              ? (
              <div className="comment-container">
                {UserStore.postComments.comments && UserStore.postComments.comments.map((list, idx) => (
                  <Col lg="12" md="12" sm="12" className="mb-3 d-inline-flex" key={idx}>
                    <img
                      className="user-comment-avatar rounded-circle mr-2"
                      src={UserStore.BASE_PHOTO_API + list.userAvatar}
                      alt="User Avatar"
                    />
                    <div className="comment-content-container">
                      <b>{list.userFullName}</b>
                      {this.state.updating == idx
                      ? (
                        <div>
                          <ReactQuill
                            id="update-input-comment"
                            ref={(el) => this.quill = el}
                            className="update-input-comment"
                            onChange={this.upadteCommentChange}
                            placeholder="Type your comment..."
                            value={this.state.updatedComment}
                            contenteditable="true"
                            modules={{
                              toolbar: {
                                container: [
                                  ['emoji']
                                ]
                              },
                              clipboard: {
                                // toggle to add extra line breaks when pasting HTML:
                                matchVisual: false,
                              },
                              'emoji-toolbar': true,
                              "emoji-textarea": true,
                              "emoji-shortname": true
                            }}
                            formats={SinglePost.formats}
                          >
                          </ReactQuill>
                          <div className="send-container">
                            <button type="button" className="invisible mx-auto p-0" onClick={() => this.sendEditedComment(list)}>
                              <h4 className="material-icons p-0 mb-0 visible">send</h4>
                            </button>
                          </div>
                        </div>
                      )
                      : (
                        <div id="comment-content">{ReactHtmlParser(list.content)}</div>
                      )}
                    </div>
                    <div className="edit-and-delete">
                      {this.state.updating == idx
                      ? (
                        <button onClick={() => this.stopEditComment(list, idx)}>Hủy Chỉnh Sửa</button>
                      )
                      : (
                        <button onClick={() => this.editComment(list, idx)}>Chỉnh Sửa</button>
                      )}
                      <button onClick={() => this.detleteComment(list)}>Xóa</button>
                    </div>
                  </Col>
                ))}
              </div>
                ) : null
              }
              {postCommentTotal
                ? (
                <PaginationComponent
                  className="pagination"
                  totalItems={postCommentTotal && postCommentTotal.total ? postCommentTotal.total : 0}
                  pageSize={postCommentTotal.limit}
                  onSelect={this.handleSelected}
                  defaultActivePage={1}
                />
                ) : null
              }
              {/* Like And Comment */}   
            </Col>
            <Col lg="4" md="4" sm="12">
              {UserStore.highLikePost
              ? (
                <Sidebar
                  avatar={require("../assets/photo/logo.png")}
                  first="Về"
                  second="Tụi Mình"
                  aboutHello="Chào mọi người! Tụi Mình Là Mỏi Chân Mòn Dép"
                  aboutDescrition="Ngoài Việc làm Travel Vlog. Tụi mình còn có kinh nghiệm trong các lĩnh vực làm video review, quảng bá cho các nhà hàng, resort, khách sạn, khu du lịch,... . 
                  Đồng thời cũng làm các bộ phim ngắn mang giá trị nhân văn cao."
                  third="Kết"
                  fourth="Nối"
                  five="Được"
                  six="Yêu Thích"
                  slideshow={UserStore.highLikePost.articles}
                  slideTotal={3}
                />
                )
                : (
                  <Sidebar
                    avatar={require("../assets/photo/logo.png")}
                    first="Về"
                    second="Tụi Mình"
                    aboutHello="Chào mọi người! Tụi Mình Là Mỏi Chân Mòn Dép"
                    aboutDescrition="Ngoài Việc làm Travel Vlog. Tụi mình còn có kinh nghiệm trong các lĩnh vực làm video review, quảng bá cho các nhà hàng, resort, khách sạn, khu du lịch,... . 
                    Đồng thời cũng làm các bộ phim ngắn mang giá trị nhân văn cao."
                    third="Follow"
                    fourth="Tụi Mình"
                    five="Được"
                    six="Yêu Thích"
                  />
                )
              }
            </Col>
        </Row>
      </div>
    )
  }
}
SinglePost.formats = [
  'emoji'
]

export default SinglePost;
