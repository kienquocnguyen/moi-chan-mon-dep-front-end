import React from "react";
import {
    Row,
    Col,
  } from "shards-react";
import Slideshow from "../Blog/Slideshow";
import SocialNetwork from "../SocialNetwork";

class Sidebar extends React.Component {
    constructor(props) {
        super(props);
    }  
    
    render() {
      return (
        <div className="single-post-sidebar">
            <div className="about-sidebar text-center d-block">
                <h3>
                    <span className="sidebar-text-first">{this.props.first}</span>
                    <span className="sidebar-text-second">{this.props.second}</span>
                </h3>
                <a href="/portfolio" target="_blank">
                    <img src={this.props.avatar} alt={this.props.alt} className="sidebar-avatar"/>
                </a>
                <p>
                    <b>{this.props.aboutHello}</b>
                    <br/>
                    {this.props.aboutDescrition}
                </p>
                <Row className="sidebar-social-container">
                    <Col lg="3" md="6" sm="3" className="text-center">
                        <SocialNetwork
                            photo={require("../../assets/photo/facebook.png")}
                            alt="test"
                            url="https://www.facebook.com/moichanmondep"
                        />
                    </Col>
                    <Col lg="3" md="6" sm="3" className="text-center">
                        <SocialNetwork
                            photo={require("../../assets/photo/instagram.png")}
                            alt="test"
                            url="https://www.instagram.com/moichanmondep/"
                        />
                    </Col>
                    <Col lg="3" md="6" sm="3" className="text-center">
                        <SocialNetwork
                            photo={require("../../assets/photo/tik-tok.png")}
                            alt="test"
                            url="https://www.tiktok.com/@moichanmondep"
                        />
                    </Col>
                    <Col lg="3" md="6" sm="3" className="text-center">
                        <SocialNetwork
                            photo={require("../../assets/photo/youtube.png")}
                            alt="test"
                            url="https://www.youtube.com/channel/UCp5Dg5p4-iCl9L7yV85zZKw"
                        />
                    </Col>
                </Row>
                <a className="read-more-sidebar" href="http://localhost:9000/portfolio">Đọc Thêm Về Tụi Mình</a>
            </div>
            <div className="about-sidebar text-center d-block">
                <h3>
                    <span className="sidebar-text-first">{this.props.five}</span>
                    <span className="sidebar-text-second">{this.props.six}</span>
                </h3>
                {this.props.slideshow
                    ? (
                    <Slideshow
                        input={this.props.slideshow}
                        length={this.props.slideTotal}
                        ratio={`16:9`}
                    />
                    )
                    : null
                }
            </div>
        </div>
      )
    }
}

export default Sidebar;