import React from "react";

class SinglePostIntro extends React.Component {
    constructor(props) {
        super(props);
    }  
    render() {

        return (
            <div className="single-post-intro" style={{ backgroundImage: `url('${this.props.backgroundImage}')` }}>
                <div className="single-post-intro-container text-center">
                    <h2>{this.props.title}</h2>
                        <span className="d-inline-block">
                            {" "}
                            <a href={this.props.authorUrl}>
                                {this.props.author}
                            </a>
                            {" | "}
                            {this.props && this.props.category ?
                                this.props.category.map(p=>
                                <a href={this.props.categoryUrl}>
                                    {p} {" "}
                                </a>
                            ) : null }
                            {" | "}
                            <a href={this.props.categoryUrl}>
                                {this.props.commentsAmount} {" "} Bình Luận
                            </a>
                        </span>
                </div>
            </div>
        )
    }
}

export default SinglePostIntro;