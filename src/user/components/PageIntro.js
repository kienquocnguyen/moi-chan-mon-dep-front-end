import React from "react";

class PageIntro extends React.Component {
    constructor(props) {
        super(props);
    }  
    
    render() {
      return (
        <div className="page-intro" style={{ backgroundImage: `url('${this.props.backgroundImage}'), linear-gradient(1deg,#000000 0%,rgba(26,26,26,.2) 100%)` }}>
            <div className="page-intro-container text-center">
                <h1>
                    <span className="page-text-first">{this.props.first}</span>
                    <span className="page-text-second">{this.props.second}</span>
                </h1>
            </div>
            <div className="intro-bottom-divinder"></div>
        </div>
      )
    }
}

export default PageIntro;