import React from "react";

class YoutubeEmbed extends React.Component {
    constructor(props) {
        super(props);
    }  
    
    render() {
        const opts = {
            height: '480',
            width: '100%'
        };
        return (
            <div className="video-responsive">
                <iframe
                    width="100%"
                    height="480"
                    src={`https://www.youtube.com/embed/${this.props.embedId}?rel=0&amp;fs=0&amp;showinfo=0`}
                    frameBorder="0"
                    allowFullScreen
                    title="Embedded youtube"
                />
            </div>
        )
    }
}

export default YoutubeEmbed;