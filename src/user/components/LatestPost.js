import React from "react";

import {
    Col,
    Card,
    CardBody,
    Badge
  } from "shards-react";
  
class LatestPost extends React.Component {
    constructor(props) {
        super(props);
      }
    render() {
      return (
        <Col lg="6" sm="12" className="mb-4">
            <Card small className="card-post card-post--aside card-post--1">
                <div
                    className="card-post__image"
                    style={{ backgroundImage: `url('${this.props.backgroundImage}')` }}
                >
                <Badge
                    pill
                    className={`card-post__category bg-${this.props.categoryTheme}`}
                >
                    {this.props.category}
                </Badge>
                <div className="card-post__author d-flex">
                    <a
                        href={this.props.postUrl}
                        className="card-post__author-avatar card-post__author-avatar--small"
                        style={{ backgroundImage: `url('${this.props.authorAvatar}')` }}
                    >
                    Written by {this.props.author}
                    </a>
                </div>
                </div>
                <CardBody>
                <h5 className="card-title latest-post-title">
                    <a className="text-fiord-blue" href={this.props.postUrl}>
                        {this.props.title}
                    </a>
                </h5>
                <p className="latest-post-description">{this.props.description}</p>
                <span className="text-muted">{this.props.date}</span>
                </CardBody>
            </Card>
        </Col>
      )
    }
}
export default LatestPost;
