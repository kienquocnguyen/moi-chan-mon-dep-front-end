import React from "react";

export default class DropdownMenu extends React.Component {

    getMenuItemTitle = (menuItem, index, depthLevel) => {
      return menuItem.title;
    };
  
    getMenuItem = (menuItem, depthLevel, index) => {
      let title = this.getMenuItemTitle(menuItem, index, depthLevel);
      
      if (menuItem.categoriesChildren && menuItem.categoriesChildren.length > 0) {
        return (
          <li>
            <a href={menuItem.slug == "blog" ? "/blog" : "/danh-muc/" + menuItem.slug}>{title}</a>
            <DropdownMenu config={menuItem.categoriesChildren} categoriesChildren={true} />
          </li>
        );
      }else {
        return <li><a href={"/danh-muc/" + menuItem.slug}>{title}</a></li>;
      }
    };
  
    render = () => {
      let { config } = this.props;
  
      let options = [];
      config.map((item, index) => {
        options.push(this.getMenuItem(item, 0, index));
      });
  
      if (this.props.categoriesChildren && this.props.categoriesChildren === true)
        return <ul>{options}</ul>;
  
      return <ul className="new-dropdown-menu">{options}</ul>;
    };
  }
  