import React from "react";
import {
    ListGroup,
    ListGroupItem,
    Col,
    Row,
    Form,
    FormInput,
    FormGroup,
    FormCheckbox,
    FormSelect,
    Button
  } from "shards-react";
class Footer extends React.Component {
    constructor(props) {
        super(props);
    }  
    
    render() {
      return (
        <div className="mt-5 footer">
            <Row>
                <Col lg="6" md="6" sm="12">
                    <div className="lets-connect">
                        <h2>Bạn Thích Những Gì Chúng Mình Làm ? Hãy Kết Nối</h2>
                        <p>Nếu bạn thích những nội dung tụi mình đang làm ? Bạn muốn tìm hiểu thêm về team "Mỏi Chân Mòn Dép" hay những nơi để khám phá.
                         Hãy kết nối với tụi mình nhé.</p>
                        <div className="connect-social-network">
                            <a href="https://www.facebook.com/moichanmondep" target="_blank">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                            <a href="https://www.instagram.com/moichanmondep" target="_blank">
                                <i class="fab fa-instagram"></i>
                            </a>
                            <a href="https://www.youtube.com/channel/UCp5Dg5p4-iCl9L7yV85zZKw" target="_blank">
                                <i class="fab fa-youtube"></i>
                            </a>
                        </div>
                    </div>
                </Col>
                <Col lg="6" md="6" sm="12">
                    <h2>Bạn Có Dự Án Muốn Hợp Tác Với Tụi Mình ? Hãy gửi thông tin cho tụi mình nhé.</h2>
                    <Form>
                        <FormGroup>
                            <label htmlFor="feInputName">Tên</label>
                            <FormInput id="feInputName" placeholder="Nhập tên của bạn." />
                        </FormGroup>

                        <FormGroup>
                            <label htmlFor="feInputEmail">Email</label>
                            <FormInput
                                id="feInputEmail"
                                placeholder="Nhập email của bạn."
                            />
                        </FormGroup>
                        <FormGroup>
                            <label htmlFor="feInputPhoneNumber">Số Điện Thoại</label>
                            <FormInput
                                id="feInputPhoneNumber"
                                placeholder="Nhập số điện thoại của bạn."
                            />
                        </FormGroup>
                        <FormGroup>
                            <label htmlFor="feDescription">Mô Tả Dự Án</label>
                            <FormInput 
                                id="feDescription"
                                rows="5"
                                placeholder="Nhập mô tả sơ lược về dự án của bạn." 
                            />
                        </FormGroup>
                        <Button type="submit">Gửi Đến Tụi Mình</Button>
                    </Form>
                </Col>
            </Row>
        </div>
      )
    }
}

export default Footer;