import React from "react";
import { ButtonGroup, Button } from "shards-react";
import UserStore from "../../MobxStore/UserStore";
import { observer } from "mobx-react";

@observer
class MainPopup extends React.Component {
    constructor(props) {
        super(props);
    }  
    
    deleteComment = async () =>{
        await UserStore.deleteComment(UserStore.deleteCommentId);
        await UserStore.getPostComments(UserStore.singlePost._id);
    }
    render() {
      return (
          <div>
            {UserStore.showPopup 
                ?(
                    <div className='popup'>
                        {UserStore.kindOfPopup == "deleteComment"
                            ? (
                                <div className='popup_inner'>
                                    <h3 className="text-center mb-4">Bạn Có Chắc Chắn Muốn Xóa Bình Luận</h3>
                                    <div className="w-100 m-auto delete-selection">
                                        <Button theme="primary" size="lg" className="delete-yes" onClick={() => this.deleteComment()}>Có</Button>
                                        <Button size="lg" className="delete-no" onClick={() => UserStore.setPopup(false)}>Không</Button>
                                    </div>
                                </div>
                            ) : null
                        }
                        {UserStore.kindOfPopup == "Unauthorized"
                            ? (
                                <div className='popup_inner'>
                                    <h3 className="text-center mb-4">{UserStore.poupMessages}</h3>
                                    <div className="w-100 m-auto delete-selection">
                                        <Button theme="primary" size="lg" className="delete-yes text-center" onClick={() => UserStore.setPopup(false)}>Ok</Button>
                                    </div>
                                </div>
                            ) : null
                        }
                    </div>
                ) : null
            }
        </div>
      )
    }
}

export default MainPopup;