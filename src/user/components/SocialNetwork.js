import React from "react";
import {
    Col,
    Row,
    Button
  } from "shards-react";

class SocialNetwork extends React.Component {
    constructor(props) {
        super(props);
    }  
    
    render() {
      return (
        <div className="mr-0 ml-0 pr-0 pl-0 text-center social-column">
          <a href={this.props.url} target="_blank" className="p-0 m-0">
            <img className="ml-auto mr-auto" src={this.props.photo} alt={this.props.alt}/>
          </a>
        </div>
      )
    }
}

export default SocialNetwork;