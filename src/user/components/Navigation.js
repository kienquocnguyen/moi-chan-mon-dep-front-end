import React from "react";
import { Link } from "react-router-dom";
import {
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    Collapse,
    Dropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem
  } from "shards-react";
import DropdownNavigation from "../components/DropdownNavigation";
import UserStore from "../../MobxStore/UserStore";

  
class Navigation extends React.Component {
    constructor(props) {
        super(props);
        this.toggleNavbar = this.toggleNavbar.bind(this);
        this.state = {
            dropdownOpen: false,
            collapseOpen: false,
            blogVisible: false,
            visible: false,
            color: "white",
            fontColor: "#5e5e5e"
        };

        this.toggleUserActions = this.toggleUserActions.bind(this);
        this.toggleBlog = this.toggleBlog.bind(this);
        this.childToggleBlog = this.childToggleBlog.bind(this);
    }

    toggleNavbar() {
        this.setState({
          ...this.state,
          ...{
            collapseOpen: !this.state.collapseOpen
          }
        });
    }

    toggleUserActions() {
        this.setState({
          visible: !this.state.visible
        });
    }

    toggleBlog() {
        this.setState({
            blogVisible: !this.state.blogVisible
        });
    }
    childToggleBlog() {
        this.setState({
            blogVisible: !this.state.blogVisible
        });
    }

    unToggleBlog() {
        this.setState({
            blogVisible: !this.state.blogVisible
        });
    }
    
    listenScrollEvent = e => {
        if (window.scrollY > 400) {
          this.setState({color: '#f66f4d'})
          this.setState({fontColor: 'white'})
        } else {
          this.setState({color: 'white'})
          this.setState({fontColor: "#5e5e5e"})
        }
    }

    userLogout = () =>{
        UserStore.logout();
    }

    componentDidMount = async () => {
        await UserStore.getCategories();
        await UserStore.logedin();
        window.scrollTo(0, 10)
        window.addEventListener('scroll', this.listenScrollEvent)
    }
    
    render() {
      return (
        <Navbar type="dark" className="navigation pl-5 pr-5" style={{backgroundColor: this.state.color}} expand="md" sticky="true">
            <NavbarBrand href="#" className="home-logo-contain" style={{color: this.state.fontColor}}>
                <img
                    className="mr-2 home-logo"
                    src={require("../assets/photo/logo.png")}
                    alt="Mỏi Chân Mòn Dép"
                />
                Mỏi Chân Mòn Dép
            </NavbarBrand>
            <NavbarToggler onClick={() => this.toggleNavbar()} />

            <Collapse open={this.state.collapseOpen} navbar>
                <Nav navbar className="navigation-item">
                    <NavItem>
                        <NavLink active href="/home" style={{color: this.state.fontColor}}>
                            Home
                        </NavLink>
                    </NavItem>
                    <NavItem tag={Dropdown} caret>
                        <DropdownNavigation
                            config={[
                            {
                                "title": "Blog",
                                "slug": "blog",
                                "categoriesChildren": UserStore.categoriesList
                            }
                            ]}
                        />
                    </NavItem>
                    <NavItem>
                        <NavLink href="/portfolio" style={{color: this.state.fontColor}}>
                            Portfolio
                        </NavLink>
                    </NavItem>
                    {localStorage.getItem("cool-jwt")
                    ? (
                        <NavItem tag={Dropdown} caret toggle={this.toggleUserActions}>
                            <DropdownToggle caret tag={NavLink} className="text-nowrap user-loggedin-menu-avatar">
                                <img
                                    className="user-avatar rounded-circle mr-2"
                                    src={UserStore.BASE_PHOTO_API + UserStore.userInfo.avatar}
                                    alt="User Avatar"
                                />{" "}
                            </DropdownToggle>
                            <Collapse tag={DropdownMenu} right small open={this.state.visible} className="dropdown-menu-avatar">
                                <DropdownItem tag={Link} onClick={this.userLogout} to="/home" className="text-danger">
                                    <i className="material-icons text-danger">&#xE879;</i> Đăng Xuất
                                </DropdownItem>
                            </Collapse>
                            {/* <NavLink href="/home" onClick={this.userLogout} style={{color: this.state.fontColor}}>
                                Đăng Xuất
                            </NavLink> */}
                        </NavItem>
                    ) : (
                        <NavItem tag={Dropdown} caret toggle={this.toggleUserActions}>
                            <DropdownToggle caret tag={NavLink} className="text-nowrap user-loggedin-menu" style={{color: this.state.fontColor}}>
                                <a>Tài Khoản</a>
                            </DropdownToggle>
                            <Collapse tag={DropdownMenu} right small open={this.state.visible}>
                                <DropdownItem tag={Link} to="/dang-nhap" style={{color: this.state.fontColor}}>
                                    Đăng Nhập
                                </DropdownItem>
                            </Collapse>
                            {/* <NavLink href="/home" onClick={this.userLogout} style={{color: this.state.fontColor}}>
                                Đăng Xuất
                            </NavLink> */}
                        </NavItem>
                    )}
                </Nav>
            </Collapse>
        </Navbar>
      )
    }
}
export default Navigation;
