import React from "react";
import {
    Col,
    Row,
    Button
  } from "shards-react";

class Intro extends React.Component {
    constructor(props) {
        super(props);
    }  
    
    render() {
      return (
        <div className="home-intro">
            <div className="mt-auto mb-auto">
                <Row>
                    <Col lg="8" md="8" sm="12" className="text-center m-auto">
                        <h4 className="short-slogan">
                            <p>
                                Cuộc sống sẽ luôn có nhũng thử thách, khó khăn mà bạn phải đối mặt trong cuộc sống.
                                Nếu bạn thật sự muốn tìm hiểu về những nơi đáng sống, những chia sẻ từ những con người
                                có thể giúp bạn có 1 cách nhìn tích cực hơn về cuộc sống. Hãy bắt đầu cùng tụi mình khám
                                phá nhé.
                            </p>
                        </h4>
                        <div className="intro-btn-contain m-auto">
                            <Button className="mb-2 mr-1 intro-about-btn">
                                About
                            </Button>
                            <Button className=" mb-2 mr-1 intro-video-btn">
                                Watch Video
                            </Button>
                        </div>
                    </Col>
                </Row>
            </div>
        </div>
      )
    }
}

export default Intro;