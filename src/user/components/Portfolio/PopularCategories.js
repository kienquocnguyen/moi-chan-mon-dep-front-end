import React from "react";
import {
    Col,
  } from "shards-react";

class PopularCategories extends React.Component {
    constructor(props) {
        super(props);
    }  
    
    render() {
      return (
        <Col lg="2" md="4" sm="4" className="mr-0 ml-0 pr-0 pl-0 overflow-hidden">
            <div className="overlay"></div>
            <div className="popular-categories">
              <p>{this.props.title}</p>
              <a href={this.props.url}>
                <div className="categories-content text-center">
                    <img src={this.props.backgroundImage} alt={this.props.alt} title={this.props.title}/>
                </div>
              </a>
            </div>
        </Col>
      )
    }
}

export default PopularCategories;