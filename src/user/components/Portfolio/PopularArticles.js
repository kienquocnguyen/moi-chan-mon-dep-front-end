import React from "react";
import {
    Col,
    Row,
    Button
  } from "shards-react";

class PopularArticles extends React.Component {
    constructor(props) {
        super(props);
    }  
    
    render() {
      return (
        <Col lg="3" md="4" sm="12" className="mb-4">
            <div className="popular-articles" style={{ backgroundImage: `url('${this.props.backgroundImage}')` }}>
                <div className="popular-content">
                    <h4 className="popular-title text-center">{this.props.title}</h4>
                    <div className="popular-description">
                        <p className="text-center">{this.props.smallTitle}</p>
                    </div>
                </div>
            </div>
        </Col>
      )
    }
}

export default PopularArticles;