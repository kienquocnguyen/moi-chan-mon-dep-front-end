import React from "react";
import {
    Col,
    Row,
    Button
  } from "shards-react";

class PortfolioDescription extends React.Component {
    constructor(props) {
        super(props);
    }  
    
    render() {
      return (
        <div className="portfolio-description text-center">
            <h1 className="mb-4">{this.props.title}</h1>
            <p>{this.props.description}</p>
        </div>
      )
    }
}

export default PortfolioDescription;