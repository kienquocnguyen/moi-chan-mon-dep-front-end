import React from "react";
import {
    Col,
    Row,
    Button
  } from "shards-react";

class TeamMembers extends React.Component {
    constructor(props) {
        super(props);
    }  
    
    render() {
      return (
        <Col lg="4" md="4" sm="4" sm="12" className="mb-4 text-center team-members">
            <img
                className="mr-2 team-avatar"
                src={this.props.avatar}
                alt="User Avatar"
            />
            <h2 className="mt-4">{this.props.name}</h2>
            <i>{this.props.position}</i>
        </Col>
      )
    }
}

export default TeamMembers;