import React from "react";
import {
    Col,
    Row,
    Button
  } from "shards-react";

class WorkingSkills extends React.Component {
    constructor(props) {
        super(props);
    }  
    
    render() {
      return (
        <div className="working-skills">
            <Row className-="working-type-container">
                <Col lg="4" md="6" sm="12" className="mb-4">
                    <div className="working-type">
                        <div className="skills">
                            <span class="material-icons-outlined">
                                photo_camera
                            </span>
                            <h3>Chụp Ảnh / Hậu Kỳ</h3>
                            <a href="#">Xem Sản Phẩm  <span class="material-icons-outlined">east</span></a>
                        </div>
                    </div>
                </Col>
                <Col lg="4" md="6" sm="12" className="mb-4">
                    <div className="working-type">
                        <div className="skills">
                            <span class="material-icons-outlined">
                                videocam
                            </span>
                            <h3>Quay / Dựng Video</h3>
                            <a href="#">Xem Sản Phẩm  <span class="material-icons-outlined">east</span></a>
                        </div>
                    </div>
                </Col>
                <Col lg="4" md="6" sm="12" className="mb-4">
                    <div className="working-type">
                        <div className="skills">
                            <span class="material-icons-outlined">
                                rate_review
                            </span>
                            <h3>Kịch Bản / Nội Dung</h3>
                            <a href="#">Xem Sản Phẩm  <span class="material-icons-outlined">east</span></a>
                        </div>
                    </div>
                </Col>
            </Row>
        </div>
      )
    }
}

export default WorkingSkills;