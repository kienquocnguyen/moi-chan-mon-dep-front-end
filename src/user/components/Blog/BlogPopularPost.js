import React from "react";
import {
    Card,
    CardBody,
    Badge
  } from "shards-react";

class BlogPopularPost extends React.Component {
    constructor(props) {
        super(props);
    }  
    
    render() {
      return (
        <Card small className="card-post card-post--aside card-post--1 blog-popular-post mb-4">
            <div
                className="card-post__image"
                style={{ backgroundImage: `url('${this.props.backgroundImage}')` }}
            >
            <Badge
                pill
                className={`card-post__category bg-${this.props.categoryTheme}`}
            >
                {this.props.category}
            </Badge>
            <div className="card-post__author d-flex">
                <a
                    href="#"
                    className="card-post__author-avatar card-post__author-avatar--small"
                    style={{ backgroundImage: `url('${this.props.authorAvatar}')` }}
                >
                    Written by {this.props.author}
                </a>
            </div>
            </div>
            <CardBody>
                <h2 className="card-title popular-post-title">
                    <a className="text-fiord-blue" href={this.props.postUrl}>
                        {this.props.title}
                    </a>
                </h2>
                <p className="popular-post-meta">
                    <span>{this.props.date}</span>
                </p>
                <p className="popular-post-content">{this.props.postContent}</p>
            </CardBody>
        </Card>
      )
    }
}

export default BlogPopularPost;