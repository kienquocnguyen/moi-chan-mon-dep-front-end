import UserStore from "../../../MobxStore/UserStore";
import React from "react";

export default class SearchArticles extends React.Component {
    constructor(props) {
      super(props);
      
      this.state = {
        suggestions: [],
        text: ''
      }
    }
    
  onTextChanged = (e) => {
    const value = e.target.value;
    let suggestions = [];
    if(value.length > 0) {
      const regex = new RegExp(`\\b${value}`, 'i');
      let data = this.props.data;
      let result = data.map(a => a.title);
      suggestions = result.sort().filter(v => regex.test(v));
    }
    this.setState(() => ({suggestions, text: value}));
  }
  
  suggestionSelected(value) {
    this.setState(() => ({
      text: value,
      suggestions: []
    }));
    UserStore.setSearchPostTile(value);
  }
  
  renderSuggestions() {
    const {suggestions} = this.state;
    if(suggestions.length === 0) {
      return null;
    }
    return (
      <div className="srchList">
        <ul>
          {suggestions.map((item) => <li onClick={() => this.suggestionSelected(item)}>{item}</li>)}
        </ul>
      </div>
    );
  }
  
    render() {
      const { text } = this.state;
      return (
        <div className="container search-articles">
          <div className="row justify-content-md-center">
            <div className="col-md-12 input">
              <input value={text} onChange={this.onTextChanged} type="text" placeholder="Tìm Kiếm Bài Viết..." />
            </div>
            <div className="col-md-12 justify-content-md-center">
              {this.renderSuggestions()}
            </div>
          </div>
        </div>
      );
    }
  }
          