import React from "react";
import {
    Col,
    Card,
    CardBody,
    CardFooter
  } from "shards-react";

class BlogArticles extends React.Component {
    constructor(props) {
        super(props);
    }  
    
    render() {
      return (
        <Col lg="4" md="6" sm="12" className="mb-5 blog-articles" key={this.props.key}>
            <Card small className="card-post h-100">
                <div
                    className="card-post__image"
                    style={{ backgroundImage: `url('${this.props.backgroundImage}')` }}
                />
                <CardBody>
                    <h5 className="card-title">
                        <a className="text-fiord-blue" href={this.props.postUrl}>
                            {this.props.title}
                        </a>
                    </h5>
                    <p className="post-date">{this.props.date}</p>
                    <p className="card-text">{this.props.postContent}</p>
                    <a className="read-more-btn" href={this.props.postUrl}>Đọc Tiếp <span class="material-icons-outlined red-more-icon">east</span></a>
                </CardBody>
                <CardFooter className="text-muted border-top py-3">
                    <span className="d-inline-block">
                    By{" "}
                        <a className="text-fiord-blue" href={this.props.authorUrl}>
                            {this.props.author}
                        </a>{" "}
                        in{" "}
                        {this.props && this.props.category ?
                            this.props.category.map(p=>
                            <a className="text-fiord-blue" href={this.props.categoryUrl}>
                                {p} {" "}
                            </a>
                        ) : null }
                    </span>
                </CardFooter>
            </Card>
        </Col>
      )
    }
}

export default BlogArticles;