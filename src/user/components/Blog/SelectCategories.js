import React from 'react';
import Multiselect from 'multiselect-react-dropdown';
import UserStore from '../../../MobxStore/UserStore';

export default class SelectCategories extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            options: [{name: 'Option 1️⃣', id: 1},{name: 'Option 2️⃣', id: 2}],
            selectedValue: []
        };
    }
    onSelect(selectedList, selectedItem) {
        UserStore.setPostCategories([...UserStore.postCategory, selectedItem])
    }
    
    onRemove(selectedList, removedItem) {
        var index = UserStore.categoriesSelected.map(x => {
            return x.id;
          }).indexOf(removedItem.id);
        UserStore.postCategory.splice(index, 1);
    }

    render() {
        return (
            <div>
                <Multiselect
                    options={this.props.items} // Options to display in the dropdown
                    selectedValues={this.state.selectedValue} // Preselected value to persist in dropdown
                    onSelect={this.onSelect} // Function will trigger on select event
                    onRemove={this.onRemove} // Function will trigger on remove event
                    displayValue="title" // Property name to display in the dropdown options
                    placeholder="Tìm theo danh mục"
                />
            </div>
        )
    }
};