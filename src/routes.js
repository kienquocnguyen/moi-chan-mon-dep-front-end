import React from "react";
import { Redirect } from "react-router-dom";

import { UserLayout } from "./user/layouts";
import { DefaultLayout } from "./admin/layouts";


//Route Admin
import BlogOverview from "./admin/views/BlogOverview";
import UserProfileLite from "./admin/views/UserProfileLite";
import AddNewPost from "./admin/views/AddNewPost";
import Errors from "./admin/views/Errors";
import ComponentsOverview from "./admin/views/ComponentsOverview";
import Tables from "./admin/views/Tables";
import BlogPosts from "./admin/views/BlogPosts";
import Categories from "./admin/views/Categories";
import PostMangement from "./admin/views/PostMangement";

//Route Views
import Home from "./user/views/Home";
import Blog from "./user/views/Blog";
import Portfolio from "./user/views/Portfolio";
import SinglePost from "./user/views/SinglePost";
import Login from "./user/views/Login";
import BlogByCategory from "./user/views/BlogByCategory";

export default [
  {
    path: "/",
    exact: true,
    layout: UserLayout,
    component: () => <Redirect to="/home" />
  },
  {
    path: "/home",
    layout: UserLayout,
    component: Home
  },
  {
    path: "/blog",
    layout: UserLayout,
    component: Blog
  },
  {
    path: "/danh-muc/:slug",
    layout: UserLayout,
    component: BlogByCategory
  },
  {
    path: "/portfolio",
    layout: UserLayout,
    component: Portfolio
  },
  {
    path: "/bai-viet/:slug",
    layout: UserLayout,
    component: SinglePost
  },
  {
    path: "/dang-nhap",
    layout: UserLayout,
    component: Login
  },
  {
    path: "/blog-overview",
    layout: DefaultLayout,
    component: BlogOverview
  },
  {
    path: "/user-profile-lite",
    layout: DefaultLayout,
    component: UserProfileLite
  },
  {
    path: "/add-new-post",
    layout: DefaultLayout,
    component: AddNewPost
  },
  {
    path: "/post-management",
    layout: DefaultLayout,
    component: PostMangement
  },
  {
    path: "/categories",
    layout: DefaultLayout,
    component: Categories
  },
  {
    path: "/errors",
    layout: DefaultLayout,
    component: Errors
  },
  {
    path: "/components-overview",
    layout: DefaultLayout,
    component: ComponentsOverview
  },
  {
    path: "/tables",
    layout: DefaultLayout,
    component: Tables
  },
  {
    path: "/blog-posts",
    layout: DefaultLayout,
    component: BlogPosts
  }
];
