import { observable, action, set, toJS } from "mobx";
import API from "./api/index";

class User {
  @observable likePostChecked = false;
  @observable showPopup = false;
  @observable poupMessages = "";
  @observable kindOfPopup = "";
  @observable showAdminPopup = false;
  @observable adminPoupMessages = "";
  @observable adminKindOfPopup = "";
  @observable userToken = "";
  @observable.ref userInfo = {};
  @observable BASE_PHOTO_API = "http://localhost:3000/uploads/";
  @observable.ref categoriesImage = {};
  @observable searchingTitle = "";
  @observable postTitle = "";
  @observable postContent = "";
  @observable postShortDescription = "";
  @observable deleteCommentId = "";
  @observable deleteCategoryId = "";
  @observable deletePosttId = "";
  @observable selectedChildCategoryId = "";
  @observable selectedParentCategoryId = "";
  @observable.ref postCategory = [];
  @observable.ref postAvatar = {};
  @observable.ref categoriesList = [];
  @observable.ref categoriesFullList = [];
  @observable.ref categoriesSelected = [];
  @observable.ref imageArray = [];
  @observable.ref singlePost = {};
  @observable postPage = 1;
  @observable postLimit = 6;
  @observable.ref postList = [];
  @observable.ref highLikePost = [];
  @observable.ref postComments = [];
  @observable postCommentPage = 1;
  @observable postCommentLimit = 3;
  

  @action setCategoriesImage = (picture) =>{
    try {
      this.categoriesImage = picture;
    } catch (error) {
      console.log(error);
    }
  }
  
  @action setPopup = (popup, kind) =>{
    this.showPopup = popup
    this.kindOfPopup = kind
  }

  @action setPopupMessages = (messages) =>{
    this.poupMessages = messages;
  }

  @action setAdminPopup = (popup, kind) =>{
    this.showAdminPopup = popup
    this.adminKindOfPopup = kind
  }

  @action setAdminPopupMessages = (messages) =>{
    this.adminPoupMessages = messages;
  }

  @action login = async body => {
      try {
        const response = await API.login(body);
        if (response.status === 200) {
          localStorage.setItem("cool-jwt", response.data.token);
          return response;
        } else {
          localStorage.removeItem("cool-jwt");
          return response;
        }
      } catch (error) {
        console.log(error);
      }
  };
  @action logedin = async () => {
      try {
        const response = await API.logedin();
        if (response) {
          if (response.status === 200) {
            this.userToken = localStorage.getItem("cool-jwt");
            this.userInfo = response.data;
            return response;
          } else {
            localStorage.removeItem("cool-jwt");
            return;
          }
        } else {
          alert("Lost internet connection");
          return;
        }
      } catch (error) {
        console.log(error);
      }
  };
  @action logout = async () => {
      try {
        localStorage.removeItem("cool-jwt");
        this.userToken = "";
        this.userInfo = {};
      } catch (error) {
        console.log("ERROR" + error);
      }
  };
  @action likePost = async (body) =>{
      try{
        const response = await API.likePost(body);
        if(response){
            if(response.status == 200){
                this.likePostChecked = true;
                return response.data;
            }else if (response.status == 401){
                return;
            }else{
                alert(response.data.message)
                return;
            }
        }else{
            alert("Lost internet connetcion");
            return;
        }
      }catch(error){
          console.log(error)
      }
  } 
  @action unLikePost = async (articleId, userId) =>{
      try{
        const response = await API.deleteLikePost(articleId, userId);
        if(response){
            if(response.status == 200){
                this.likePostChecked = false;
                return response.data;
            }else if (response.status == 401){
                return;
            }else{
                alert(response.data.message)
                return;
            }
        }else{
            alert("Lost internet connetcion");
            return;
        }
      }catch(error){
          console.log(error)
      }
    } 
  @action checkLikedPost = async (id) =>{
      try{
          const response = await API.checkLikePost(id);
          if(response){
              if(response.status == 200){
                  this.likePostChecked = response.data.checkLiked;
                  return response.data;
              }else if (response.status == 401){
                  return;
              }else{
                  alert(response.data.message)
                  return;
              }
          }else{
              alert("Lost internet connetcion");
              return;
          }
      }catch(error){
          console.log(error)
      }
  }
  /* Post Comment */
  @action createComment = async (body) =>{
    try{
        const response = await API.newComment(body);
        if(response){
            if(response.status == 200){
                return response;
            }else{
                return response;
            }
        }
        else{
            alert("Lost internet connection");
            return;
        }
    }catch(error){
        console.log(error)
    }
  }
  @action getPostComments = async (id) => {
    try {
      const response = await API.postComment(id);
      if (response) {
        if (response.status === 200) {
          this.postComments = response.data;
          return response.data;
        } else if (response.status === 404) {
          this.postComments = [];
          return;
        } else {
          alert(response.data.message);
          return;
        }
      } else {
        alert("Lost internet connection");
        return;
      }
    } catch (error) {
      console.log(error);
    }
  };
  @action setPostCommentPage = (page, litmit) => {
    this.postCommentPage = page;
    this.postCommentLimit = litmit;
  };
  @action editComment = async (body) =>{
    try{
      const response = await API.updateComment(body);
      if(response){
          if(response.status == 200){
            return response.data;
          }else{
            UserStore.setPopup(true, "Unauthorized");
            UserStore.setPopupMessages(response.data.message);
            return;
          }
      }
      else{
          alert("Lost internet connection");
          return;
      }
    }catch(error){
        console.log(error)
    }
  }
  @action setDeleteCommentId = (id) =>{
    try{
      this.deleteCommentId = id;
    }catch(error){
      console.log(error)
    }
  }
  @action deleteComment = async (id) =>{
    try{
      const response = await API.removeComment(id);
      if(response){
        if(response.status == 200){
          UserStore.setPopup(false);
          return response.data;
        }else{
          UserStore.setPopup(true, "Unauthorized");
          UserStore.setPopupMessages(response.data.message);
          return;
        }
      }
      else{
        alert("Lost internet connection");
        return;
      }
    }catch(error){
        console.log(error)
    }
  }
  /* Post Handle */
  @action setPostTitle = (title) => {
    try {
      this.postTitle = title;
    } catch (error) {
      console.log(error);
    }
  }
  @action setPostContent = (body) =>{
    try {
      this.postContent = body;
    } catch (error) {
      console.log(error);
    }
  }
  @action setPostShortDescription = (body) =>{
    try {
      this.postShortDescription = body;
    } catch (error) {
      console.log(error);
    }
  }
  @action setPostAvatar = (picture) => {
    try {
      this.postAvatar = picture;
    } catch (error) {
      console.log(error);
    }
  }
  @action uploadPost = async (body) =>{
    try{
      const response = await API.uploadArticles(body);
      if(response){
          if(response.status == 200){
              alert("Success");
              return response.data;
          }else if (response.status == 401){
              alert(response.data.message);
              return;
          }else{
              alert(response.data.message)
              return;
          }
      }else{
          alert("Lost internet connetcion");
          return;
      }
    }catch(error){
        console.log(error)
    }
  }
  @action setImageArray = async (data) =>{
    try{
      this.imageArray = data
    }catch(error){
        console.log(error)
    }
  }
  @action setDeletePostId = async (id) =>{
    try{
      this.deletePosttId = id;
    }catch(error){
      console.log(error)
    }
  }
  @action deletePost = async (id) =>{
    try{
      const response = await API.removePost(id);
      if(response){
        if(response.status == 200){
          UserStore.setAdminPopup(false);
          return response.data;
        }else{
          UserStore.setAdminPopup(true, "notification");
          UserStore.setAdminPopupMessages(response.data.message);
          return;
        }
      }
      else{
        alert("Lost internet connection");
        return;
      }
    }catch(error){
        console.log(error)
    }
  }
  //GET POST HANDLE
  @action getSinglePost = async (slug) =>{
    try {
      const response = await API.singlePost(slug);
      if (response) {
        if (response.status === 200) {
          this.singlePost = response.data[0];
          return response.data;
        } else {
          alert(response.data.message);
          return;
        }
      } else {
        alert("Lost internet connection");
        return;
      }
    } catch (error) {
      console.log(error);
    }
  }
  @action setPostPage = (page, litmit) => {
    this.postPage = page;
    this.postLimit = litmit;
  };
  @action setSearchPostTile = (title) => {
    this.searchingTitle = title;
  }
  @action getAllPost = async (withQuery) =>{
    try {
      let response = await API.findAllPost();
      if(withQuery){
        response = await API.findAllPost(withQuery)
      }
      if (response) {
        if (response.status === 200) {
          this.postList = response.data;
          this.searchingTitle = ""
          return response.data;
        }else{
          UserStore.setPopup(true, "Unauthorized");
          UserStore.setPopupMessages(response.data.message);
        }
      } else {
        return;
      }
      return response;
    } catch (error) {
      console.log(error);
    }
  }
  @action getLikePost = async (page, limit) =>{
    try {
      const response = await API.findPostLike(page, limit);
      if (response) {
        if (response.status === 200) {
          this.highLikePost = response.data;
          return response.data;
        }
      } else {
        return;
      }
      return response;
    } catch (error) {
      console.log(error);
    }
  }
  /* Categories Handle */
  @action createCategory = async body =>{
    try{
      const response = await API.newCategories(body);
      if(response){
          if(response.status == 200){
            UserStore.setAdminPopupMessages(response.data.message);
            UserStore.setAdminPopup(true, "notification");
            return response.data;
          }else{
            UserStore.setAdminPopupMessages(response.data.message);
            UserStore.setAdminPopup(true, "notification");
            return;
          }
      }else{
          alert("Lost internet connetcion");
          return;
      }
    }catch(error){
        console.log(error)
    }
  }
  @action getCategories = async () =>{
    try{
      const response = await API.getAllCategories();
      if(response){
          if(response.status == 200){
            this.categoriesList = response.data.articleCategories
          }else{
            this.categoriesList = null
            return;
          }
      }else{
          alert("Lost internet connetcion");
          return;
      }
    }catch(error){
        console.log(error)
    }
  }
  @action getFullCategories = async () =>{
    try{
      const response = await API.getFullListCategories();
      if(response){
          if(response.status == 200){
            this.categoriesFullList = response.data.articleCategories
          }else{
            this.categoriesFullList = null
            return;
          }
      }else{
          alert("Lost internet connetcion");
          return;
      }
    }catch(error){
        console.log(error)
    }
  }
  @action getCategoryBySlug = async (slug) =>{
    try {
      const response = await API.singleCategory(slug);
      if (response) {
        if (response.status === 200) {
          this.postCategory.push(response.data[0]);
          return response.data[0];
        } else {
          UserStore.setPopup(true, "Unauthorized");
          UserStore.setPopupMessages(response.data.message);
          return;
        }
      } else {
        alert("Lost internet connection");
        return;
      }
    } catch (error) {
      console.log(error);
    }
  }
  @action setPostCategories = (categories) => {
    try {
      this.postCategory = categories;
    } catch (error) {
      console.log(error);
    }
  }
  @action selectCategories = async (data) =>{
    try{
      this.categoriesSelected = data;
    }catch(error){
        console.log(error)
    }
  }
  @action setDeleteCategoryId = async (id) =>{
    try{
      this.deleteCategoryId = id;
    }catch(error){
      console.log(error)
    }
  }
  @action deleteCategory = async (id) =>{
    try{
      const response = await API.removeCategory(id);
      if(response){
        if(response.status == 200){
          UserStore.setAdminPopup(false);
          return response.data;
        }else{
          UserStore.setAdminPopup(true, "notification");
          UserStore.setAdminPopupMessages(response.data.message);
          return;
        }
      }
      else{
        alert("Lost internet connection");
        return;
      }
    }catch(error){
        console.log(error)
    }
  }
  @action setSelectedParentCategory = (childId, parentId) =>{
    try{
      this.selectedChildCategoryId = childId;
      this.selectedParentCategoryId = parentId;
    }catch(error){
      console.log(error)
    }
  }
  @action updateCategoryParent = async () =>{
    try{
      const response = await API.updateParentCategory(this.selectedParentCategoryId, this.selectedChildCategoryId);
      if(response){
          if(response.status == 200){
            UserStore.setAdminPopupMessages(response.data.message);
            UserStore.setAdminPopup(true, "notification");
            return response.data;
          }else{
            UserStore.setAdminPopupMessages(response.data.message);
            UserStore.setAdminPopup(true, "notofication");
            return;
          }
      }
      else{
          alert("Lost internet connection");
          return;
      }
    }catch(error){
        console.log(error)
    }
  }
}
const UserStore = new User();
export default UserStore;