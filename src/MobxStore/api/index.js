import { post, get, put, del } from "./baseAPI";
import UserStore from "../UserStore";
const BASE_API_DEV = "http://localhost:3000/";

const BaseURL = BASE_API_DEV;

export default class ServerApi {
    //POST API
    static login = body => {
        const url = BaseURL + "login";
        const header = {
        "Content-Type": "application/json"
        };
        return post(url, body, header);
    };
    static logedin = () => {
        const url = BaseURL + "loggedin";
        const header = {
          "Content-Type": "application/json",
          Authorization: "Bearer" + " " + localStorage.getItem("cool-jwt")
        };
        return get(url, header);
      };
    static likePost = (body) =>{
        const url = BaseURL + `articleLike`;
        const header = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + ' ' + localStorage.getItem("cool-jwt")
        };
        return post(url, body, header);
    }
    static checkLikePost = (id) =>{
        const url = BaseURL + `articleLike/checkLiked/` + id;
        const header = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + ' ' + localStorage.getItem("cool-jwt")
        };
        return get(url, header);
    }
    static deleteLikePost = (articleId, userId) =>{
        const params = {
            articleId: articleId,
            userId: userId
        }
        const url = BaseURL + `articleLike/delete`;
        const header = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + ' ' + localStorage.getItem("cool-jwt")
        };
        return del(url, header, params);
    }
    static newComment = body => {
        const url = BaseURL + `ariticleComment`;
        const header = {
          "Content-Type": "application/json",
          Authorization: "Bearer" + " " + localStorage.getItem("cool-jwt")
        };
        return post(url, body, header);
    };
    static postComment = (id) => {
        const params = {
          postId: id,
          page: UserStore.postCommentPage,
          limit: UserStore.postCommentLimit
        };
        const url = BaseURL + "article/ariticleComment";
        const header = {
          "Content-Type": "application/json"
        };
        return get(url, header, params);
    };
    static updateComment = (body) => {
        const url = BaseURL + `ariticleComment/update/` + body._id;
        const header = {
          "Content-Type": "application/json",
          Authorization: "Bearer" + " " + localStorage.getItem("cool-jwt")
        };
        return put(url, body, header);
    };
    static removeComment = (id) => {
        const url = BaseURL + `ariticleComment/delete/` + id;
        const header = {
          "Content-Type": "application/json",
          Authorization: "Bearer" + " " + localStorage.getItem("cool-jwt")
        };
        return del(url, header);
    };
    static singlePost = slug => {
        const url = BaseURL + "articles/" + slug;
        const header = {
          "Content-Type": "application/json"
        };
        return get(url, header);
    };
    static findAllPost = (withQuery) => {
        let categoriesParams = [];
        if(Array.isArray(UserStore.postCategory)){
            let articleCategories = [];
            for(var i = 0; i < UserStore.postCategory.length; i ++){
                articleCategories.push(UserStore.postCategory[i].title)
            }
            let convertCategoriesToObject = {articleCategories}
            categoriesParams = Object.keys(convertCategoriesToObject).map(key => {
                if (Array.isArray(convertCategoriesToObject[key])) {
                return convertCategoriesToObject[key].map((value) => `${key}[]=${value}`).join('&')
                }
            })
        }else{
            categoriesParams.push("articleCategories[]=" + UserStore.postCategory.title.toString())
        }

        let url=""
        if(withQuery){
            url = BaseURL + "articles?" + "page=" + UserStore.postPage + "&limit=" + UserStore.postLimit + "&title=" + UserStore.searchingTitle + "&" + categoriesParams;
        }else{
            url = BaseURL + "articles?" + "page=" + UserStore.postPage + "&limit=" + UserStore.postLimit;
        }
        const header = {
            "Content-Type": "application/json"
        };
        return get(url, header);
    };
    static findPostLike = (page, limit) => {
        const params = {
            page: page,
            limit: limit
        };
        const url = BaseURL + "like/articles";
        const header = {
            "Content-Type": "application/json"
        };
        return get(url, header, params);
    };
    static removePost = (id) => {
        const url = BaseURL + `delete/articles/` + id;
        const header = {
          "Content-Type": "application/json",
          Authorization: "Bearer" + " " + localStorage.getItem("cool-jwt")
        };
        return del(url, header);
    };
    /* Categories Handle */
    static newCategories = (body) =>{
        const url = BaseURL + `articleCategories`;
        const header = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + ' ' + UserStore.userToken
        };
        return post(url, body, header);
    }
    static getAllCategories = () =>{
        const url = BaseURL + `articleCategories/list`;
        const header = {
            'Content-Type': 'application/json'
        };
        return get(url, header);
    }
    static getFullListCategories = () =>{
        const url = BaseURL + `articleCategories/fulllist`;
        const header = {
            'Content-Type': 'application/json'
        };
        return get(url, header);
    }
    static singleCategory = slug => {
        const url = BaseURL + "articleCategories/" + slug;
        const header = {
          "Content-Type": "application/json"
        };
        return get(url, header);
    };
    static removeCategory = (id) => {
        const url = BaseURL + `articleCategories/delete/` + id;
        const header = {
          "Content-Type": "application/json",
          Authorization: "Bearer" + " " + localStorage.getItem("cool-jwt")
        };
        return del(url, header);
    };
    static updateParentCategory = (parentId, categoryId) => {
        const params = {
            parentId: parentId,
            categoryId: categoryId
        }
        const body = {};
        const url = BaseURL + `articleCategories/update?` + "parentId=" + params.parentId + "&categoryId=" + params.categoryId;
        const header = {
          "Content-Type": "application/json",
          Authorization: "Bearer" + " " + localStorage.getItem("cool-jwt")
        };
        return put(url, body, header);
    };
    /* Post Handle */
    static uploadArticles = (body) =>{
        const url = BaseURL + `articles`;
        const header = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + ' ' + UserStore.userToken
        };
        return post(url, body, header);
    }
}