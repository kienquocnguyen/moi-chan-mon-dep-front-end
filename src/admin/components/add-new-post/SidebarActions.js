/* eslint jsx-a11y/anchor-is-valid: 0 */

import React from "react";
import PropTypes from "prop-types";
import {
  Card,
  CardHeader,
  CardBody,
  ListGroup,
  ListGroupItem,
  Button
} from "shards-react";
import UserStore from "../../../MobxStore/UserStore";
import moment from "moment";
import _ from "lodash";
import JSSoup from 'jssoup'; 

class SidebarActions extends React.Component {
  constructor(props) {
    super(props);
  }
  base64tofile = (dataurl, filename) => {
 
    var arr = dataurl.split(','),
        mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]), 
        n = bstr.length, 
        u8arr = new Uint8Array(n);
        
    while(n--){
        u8arr[n] = bstr.charCodeAt(n);
    }
    
    return new File([u8arr], filename, {type:mime});
  }

  //Collect image from array that have the same name with image in richtext editor
  imageHasValue = (value) =>{
    let usingImage = [];
    for(var i = 0; i < UserStore.imageArray.length; i++){
      if(_.find(UserStore.imageArray, {name: value[i]})){
        usingImage.push(_.find(UserStore.imageArray, {name: value[i]}));
      }
    }
    return usingImage;
  }
  //Replace image src from base64 to file name
  replaceImageSrc = (data) =>{
    var soup = new JSSoup(data);
    var tag = soup.findAll('img');
    let usingImageName = [];
    for (var i = 0; i < tag.length; i++){
      if(soup.findAll('img')){
        usingImageName.push(soup.findAll('img')[i].attrs.class)
      }
    }
    UserStore.setImageArray(this.imageHasValue(usingImageName));
    for (var i = 0; i < tag.length; i++){
      if(soup.findAll('img')){
        soup.findAll('img')[i].attrs.src = "http://localhost:3000/uploads/" + UserStore.imageArray[i].name;
      }
    }
    return soup.toString();
  }
  //Upload entire post
  uploadPost = async () =>{
    const postDate = moment(new Date()).toISOString();
    let postData = new FormData();
    postData.append("authorId", UserStore.userInfo._id);
    postData.append("title", UserStore.postTitle);

    //Replace Image Src from base64 to file name
    let postContent = this.replaceImageSrc(UserStore.postContent);
    UserStore.setPostContent(postContent)
    postData.append("description", UserStore.postContent);
    postData.append("shortDescription", UserStore.postShortDescription);

    postData.append("avatar", UserStore.postAvatar[0]);
    postData.append("writtingDate", postDate);
    for(var i = 0; i < UserStore.imageArray.length; i++){
      postData.append("image", UserStore.imageArray[i].file);
    }
  
    //append article categories array
    let categoriesArray = UserStore.categoriesSelected;
    if(UserStore.categoriesSelected.length < 2){
      categoriesArray.push("justtest")
    }
    for(let i = 0; i < categoriesArray.length; i++)
    {
      postData.append(`articleCategories`, categoriesArray[i]);
    }
    await UserStore.uploadPost(postData)

  }
  render() {
    return (
      <Card small className="mb-3">
        <CardHeader className="border-bottom">
          <h6 className="m-0">Trạng Thái</h6>
        </CardHeader>

        <CardBody className="p-0">
          <ListGroup flush>
            <ListGroupItem className="p-3">
              <span className="d-flex mb-2">
                <i className="material-icons mr-1">flag</i>
                <strong className="mr-1">Status:</strong> Draft{" "}
                <a className="ml-auto" href="#">
                  Edit
                </a>
              </span>
              <span className="d-flex mb-2">
                <i className="material-icons mr-1">visibility</i>
                <strong className="mr-1">Visibility:</strong>{" "}
                <strong className="text-success">Public</strong>{" "}
                <a className="ml-auto" href="#">
                  Edit
                </a>
              </span>
              <span className="d-flex mb-2">
                <i className="material-icons mr-1">calendar_today</i>
                <strong className="mr-1">Schedule:</strong> Now{" "}
                <a className="ml-auto" href="#">
                  Edit
                </a>
              </span>
              <span className="d-flex">
                <i className="material-icons mr-1">score</i>
                <strong className="mr-1">Readability:</strong>{" "}
                <strong className="text-warning">Ok</strong>
              </span>
            </ListGroupItem>
            <ListGroupItem className="d-flex px-3 border-0">
              <Button outline theme="accent" size="sm">
                <i className="material-icons">save</i> Save Draft
              </Button>
              <Button theme="accent" size="sm" className="ml-auto" onClick={this.uploadPost}>
                <i className="material-icons">file_copy</i> Publish
              </Button>
            </ListGroupItem>
          </ListGroup>
        </CardBody>
      </Card>
    )
  }
}

export default SidebarActions;
