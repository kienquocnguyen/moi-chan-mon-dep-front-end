import React from "react";
import ImageUploader from "react-images-upload";
import {
  Card,
  CardHeader,
  CardBody,
  ListGroup,
  ListGroupItem,
  Button,
  InputGroup,
  InputGroupAddon,
  FormCheckbox,
  FormInput
} from "shards-react";
import UserStore from "../../../MobxStore/UserStore";
import PaginationComponent from "react-reactstrap-pagination";
import { observer } from "mobx-react";
import CheckBoxCategories from "./CheckBoxCategories";


@observer
class SidebarCategories extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      description: "",
      selectedPage: 1,
      checkedItems: new Map(),
      avatar: [],
      categories:[]
    };

    this.handleChange = this.handleChange.bind(this);
  }

  onDropEventavatar = picture => {
    UserStore.setCategoriesImage(picture);
  };
  handleCategoryName = (e) =>{
    this.setState({title: e.target.value});
  }
  handleCategoryDescription = (e) =>{
    this.setState({description: e.target.value});
  }
  addNewCategories = async () =>{
    const categories = new FormData();
    categories.append("title", this.state.title);
    categories.append("description", this.state.description);
    categories.append("avatar", UserStore.categoriesImage[0]);
    await UserStore.createCategory(categories);
  }

  componentDidMount = async () =>{
    await UserStore.getCategories();
  }

  //Checkbox Handle
  handleChange = e => {
    const item = e.target.name;
    const isChecked = e.target.checked;
    this.setState(prevState => ({
        checkedItems: prevState.checkedItems.set(item, isChecked)
    }));
  };
  applyCategories = () =>{
    let data = this.state.checkedItems;
    const obj = Object.fromEntries(data);
    const categories = Object.keys(obj).filter((item) => {
      return obj[item] === true
    });
    UserStore.selectCategories(categories);
    alert("Applied");
  }

  deleteCheckboxState = (name, checked) => {
      const updateChecked = checked == null ? true : false;
      this.setState(prevState => prevState.checkedItems.set(name, updateChecked));
  };

  
  render(){
    return(
      <Card small className="mb-3">
        <CardHeader className="border-bottom">
          <h6 className="m-0">Categories</h6>
        </CardHeader>
        <CardBody className="p-0">
          <ListGroup flush>
            <ListGroupItem className="px-3 pb-2">
            {UserStore.categoriesList
            ? (
                <div>
                  {UserStore.categoriesList && UserStore.categoriesList.map((list, idx) => (
                    <div>
                      <CheckBoxCategories
                        name={list.title}
                        checked={this.state.checkedItems.get(list.title) || false}
                        onChange={this.handleChange}
                        key={idx}
                        class="ml-1"
                      />
                      {list.categoriesChildren
                        ? (
                          <div>
                            {list.categoriesChildren.map((child, childIdx) => (
                              <div>
                                <CheckBoxCategories
                                  name={child.title}
                                  checked={this.state.checkedItems.get(child.title) || false}
                                  onChange={this.handleChange}
                                  key={childIdx}
                                  class="ml-3"
                                />
                                {child.categoriesChildren
                                  ? (
                                    <div>
                                      {child.categoriesChildren.map((subChild, subChildIdx) => (
                                        <CheckBoxCategories
                                          name={subChild.title}
                                          checked={this.state.checkedItems.get(subChild.title) || false}
                                          onChange={this.handleChange}
                                          key={subChildIdx}
                                          class="ml-3"
                                        />
                                      ))}
                                    </div>
                                  ) : null
                                }
                              </div>
                            ))}
                          </div>
                        ) : null
                      }
                    </div>
                  ))}
                </div>
              )
              : null
            }  
            <Button theme="primary" className="mt-2 w-100" onClick={this.applyCategories}>
              Apply
            </Button>
            </ListGroupItem>
    
            <ListGroupItem className="d-block px-3">
              <InputGroup className="ml-auto mt-2">
                <FormInput placeholder="Tên Danh Mục Mới" onChange={(e) => this.handleCategoryName(e)}/>
              </InputGroup>
              <FormInput placeholder="Mô Tả Danh Mục Mới" className="mt-2" onChange={(e) => this.handleCategoryDescription(e)}/>
            </ListGroupItem>
            <ListGroupItem className="d-flex px-3">
              <ImageUploader
                withIcon={true}
                buttonText="Choose images"
                onChange={this.onDropEventavatar}
                imgExtension={[".jpg", ".gif", ".png", ".gif"]}
                singleImage={true}
                withPreview={true}
              />
            </ListGroupItem>
            <ListGroupItem className="d-block px-3">
              <Button theme="primary" className="mt-2 w-100" onClick={this.addNewCategories}>
                  Thêm Danh Mục Bài Viết
              </Button>
            </ListGroupItem>
          </ListGroup>
        </CardBody>
      </Card>
    )
  }
}


export default SidebarCategories;
