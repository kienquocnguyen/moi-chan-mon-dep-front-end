import React, { cloneElement, useState } from "react";
import dynamic from 'next/dynamic';
import { Card, CardBody, Form, FormInput, Button } from "shards-react";
import "react-quill/dist/quill.snow.css";
import "../../assets/quill.css";
import UserStore from "../../../MobxStore/UserStore";
import moment from "moment";
import _, { entriesIn } from "lodash";
import { observer } from "mobx-react";

import {Quill} from "react-quill"
const ReactQuill = dynamic(() => import('react-quill'), { ssr: false });
var Image = Quill.import('formats/image');
Quill.register(Image, true);

@observer
class UpdateEditor extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        body: "",
        shortBody: "",
        myArray: [],
    };
    this.handleChange = this.handleChange.bind(this);
  }

  imageHandler() {
    const input = document.createElement('input');

    input.setAttribute('type', 'file');
    input.setAttribute('accept', 'image/*');
    input.click();
    input.onchange = () => {
        const file = input.files;
        const formData = new FormData();

        formData.append('image', file);
        const quillIndex = this.quill.getSelection();
        this.getBase64 = (file) => {
          return new Promise(resolve => {
            let reader = new FileReader();
            // Convert the file to base64 text
            reader.readAsDataURL(file);
      
            // on reader load somthing...
            reader.onload = () => {
      
              // Make a fileInfo Object
              let fileInfo = {
                name: file.name,
                index: quillIndex.index,
                type: file.type,
                size: Math.round(file.size / 1000) + ' kB',
                base64: reader.result,
                file: file,
              };
              resolve(fileInfo);
            }
          });
        };
        for(var i = 0; i < file.length; i++){
          this.getBase64(file[i])
          .then(result => {
            //add image name to class so later you can use that for changing image src
            Image.className = result.name;
            UserStore.imageArray.push(result);
            let imageIndex = UserStore.imageArray.length - 1;
            this.quill.insertEmbed(quillIndex.index, 'image', UserStore.imageArray[imageIndex].base64);
          })
          .catch(err => {
            console.log(err);
          });
        }
    };
  }
  getImgUrls = (delta) => {
    return delta.filter(i => i.insert && i.insert.image).map(i => i.insert.image);
  }
  handleBody = (content, delta, source, editor) => {
    UserStore.setPostContent(content);
  }
  shortDesChange (e) {
    UserStore.setPostShortDescription(e.target.value)
  }
  handleChange (e) {
    UserStore.setPostTitle(e.target.value)
  }

  render(){
    const singlePost = this.props.singlePost ? this.props.singlePost : {};
    return(
      <Card small className="mb-3">
        <CardBody>
          <Form className="add-new-post">
            <FormInput size="lg" className="mb-3" placeholder="Your Post Title" onChange={(e) => {this.handleChange(e)}} value={singlePost ? singlePost.title : ""}/>
            <textarea size="lg" id="txtShortDescription" className="mb-3" placeholder="Short Description" value={singlePost ? singlePost.shortDescription : ""} onChange={(e) => {this.shortDesChange(e)}}/>
            <ReactQuill 
              className="add-new-post__editor mb-1"
              ref={el => {
                this.quill = el;
              }}
              modules={{
                toolbar: {
                  container: [
                    [{header: [1, 2, 3, 4, 5, 6] }, { font: [] } ],
                    [{ size: [] }],
                    ["bold", "italic", "underline", "strike", "blockquote"],
                    [{ list: "ordered" }, { list: "bullet" }],
                    ["link", "image", "video"],
                    ["clean"],
                    ["code-block"],
                  ],
                  handlers: {
                    image: this.imageHandler
                  },
                }
              }}
              formats={UpdateEditor.formats}
              placeholder="Add a description of your post"  
              onChange={this.handleBody}
              value={singlePost ? singlePost.description : ""} 
              id="txtDescription"   
            />
          </Form>
        </CardBody>
      </Card>
    )
  }
}
UpdateEditor.formats = [
  "header",
  "font",
  "size",
  "bold",
  "italic",
  "underline",
  "strike",
  "blockquote",
  "list",
  "bullet",
  "link",
  "image",
  "video",
  "code-block",
];
export default UpdateEditor;
