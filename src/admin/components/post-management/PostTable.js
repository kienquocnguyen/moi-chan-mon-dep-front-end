import React from "react";
import { Container, Row, Col, Card, CardHeader, CardBody, Button } from "shards-react";
import UserStore from "../../../MobxStore/UserStore";
import AdminPopup from "../AdminPopup";
import { observer } from "mobx-react";

@observer
class PostTable extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
    }
  }
  componentDidMount = async () =>{
    UserStore.setPostPage(1, 6);
    const postResponse  = await UserStore.getAllPost(false);
    if(postResponse){
        await UserStore.getFullCategories();
        return;
    }else{
        alert("Lost internet connection")
    }
  }
  deletePost = (id) =>{
    UserStore.setDeletePostId(id)
    UserStore.setAdminPopupMessages("Bạn có chắc là sẽ xóa bài viết này không ?")
    UserStore.setAdminPopup(true, "deletePost")
  }

  editPost = async (slug) =>{
    const getSinglePost = await UserStore.getSinglePost(slug);
    if(getSinglePost[0] == null){
      return;
    }
  }
  render() {
    const postList = UserStore.postList ? UserStore.postList.articles : null;
    const postTotal = UserStore.postList.total;
    return (
        <Row className="post-table-container">
            {UserStore.showAdminPopup
            ? (
                <AdminPopup/>
            ) : null
            }
            <Col>
                <Card small className="mb-4">
                    <CardHeader className="border-bottom">
                        <h6 className="m-0">Active Users</h6>
                    </CardHeader>
                    <CardBody className="p-0 pb-3">
                            <table className="table mb-0">
                                <thead className="bg-light">
                                    <tr>
                                    <th scope="col" className="border-0" style={{width: "10%"}}>
                                        Avatar
                                    </th>
                                    <th scope="col" className="border-0" style={{width: "25%"}}>
                                        Title
                                    </th>
                                    <th scope="col" className="border-0" style={{width: "40%"}}>
                                        Short Description
                                    </th>
                                    <th scope="col" className="border-0" style={{width: "25%"}}>
                                        Control
                                    </th>
                                    </tr>
                                </thead>
                                {UserStore.postList
                                    ? (
                                        <tbody>
                                            {postList && postList.map((list, idx) => (
                                                <tr>
                                                    <td>
                                                        <img
                                                            src={UserStore.BASE_PHOTO_API + list.avatar}
                                                            className="post-table-avatar"
                                                            alt={list.slug}
                                                        />
                                                    </td>
                                                    <td>{list.title}</td>
                                                    <td>{list.shortDescription}</td>
                                                    <td>
                                                        <div className="m-auto text-center">
                                                            <Button theme="primary" className="mr-3 edit-post-table" onClick={() => this.editPost(list.slug)}>Edit</Button>
                                                            <Button theme="danger" className="delete-post-table" onClick={() => this.deletePost(list._id)}>Delete</Button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            ))}
                                        </tbody>
                                    )
                                    : null
                                }
                            </table>
                    </CardBody>
                </Card>
            </Col>
        </Row>
    )
  }
}

export default PostTable;
