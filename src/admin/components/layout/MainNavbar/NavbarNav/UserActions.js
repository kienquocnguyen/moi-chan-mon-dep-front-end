import React from "react";
import { Link } from "react-router-dom";
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Collapse,
  NavItem,
  NavLink
} from "shards-react";
import UserStore from "../../../../../MobxStore/UserStore";
import { observer } from "mobx-react";
import {
	withRouter
} from 'react-router-dom';

@observer
class UserActions extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      visible: false
    };

    this.toggleUserActions = this.toggleUserActions.bind(this);
  }

  toggleUserActions() {
    this.setState({
      visible: !this.state.visible
    });
  }

  adminLogout(){
    UserStore.logout();
    this.props.history.push('/home');
  }
  componentDidMount = async () => {
    const user = await UserStore.logedin();
    if(UserStore.userToken == ""){
      this.props.history.push('/home');
    }else{
      if(user.status == 401){
        this.props.history.push('/home');
      }else{
        return;
      }
    }
  }

  render() {
    return (
      <NavItem tag={Dropdown} caret toggle={this.toggleUserActions}>
        <DropdownToggle caret tag={NavLink} className="text-nowrap px-3">
          <img
            className="user-avatar rounded-circle mr-2"
            src={UserStore.BASE_PHOTO_API + UserStore.userInfo.avatar}
            alt="User Avatar"
          />{" "}
          <span className="d-none d-md-inline-block">{UserStore.userInfo.firstName + " " + UserStore.userInfo.lastName}</span>
        </DropdownToggle>
        <Collapse tag={DropdownMenu} right small open={this.state.visible}>
          <DropdownItem tag={Link} to="user-profile">
            <i className="material-icons">&#xE7FD;</i> Profile
          </DropdownItem>
          <DropdownItem tag={Link} to="edit-user-profile">
            <i className="material-icons">&#xE8B8;</i> Edit Profile
          </DropdownItem>
          <DropdownItem divider />
          <DropdownItem tag={Link} onClick={this.adminLogout} to="/" className="text-danger">
            <i className="material-icons text-danger">&#xE879;</i> Logout
          </DropdownItem>
        </Collapse>
      </NavItem>
    );
  }
}

export default withRouter(UserActions);
