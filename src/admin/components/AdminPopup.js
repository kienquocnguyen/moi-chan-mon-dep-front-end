import React from "react";
import { ButtonGroup, Button } from "shards-react";
import UserStore from "../../MobxStore/UserStore";
import { observer } from "mobx-react";

@observer
class AdminPopup extends React.Component {
    constructor(props) {
        super(props);
    }  
    
    deleteCategory = async () =>{
        await UserStore.deleteCategory(UserStore.deleteCategoryId);
        await UserStore.getCategories();
    }
    deletePost = async () =>{
        await UserStore.deletePost(UserStore.deletePosttId);
        await UserStore.getAllPost(false);
    }
    render() {
      return (
        <div>
            {UserStore.showAdminPopup 
                ?(
                    <div className='popup'>
                        {UserStore.adminKindOfPopup == "deleteCategoreis"
                            ? (
                                <div className='popup_inner'>
                                    <h3 className="text-center mb-4">{UserStore.adminPoupMessages}</h3>
                                    <div className="w-100 m-auto delete-selection">
                                        <Button theme="primary" size="lg" className="delete-yes" onClick={() => this.deleteCategory()}>Có</Button>
                                        <Button size="lg" className="delete-no" onClick={() => UserStore.setAdminPopup(false)}>Không</Button>
                                    </div>
                                </div>
                            ) : null
                        }
                        {UserStore.adminKindOfPopup == "notification"
                            ? (
                                <div className='popup_inner'>
                                    <h3 className="text-center mb-4">{UserStore.adminPoupMessages}</h3>
                                    <div className="w-100 m-auto delete-selection">
                                        <Button theme="primary" size="lg" className="delete-yes text-center" onClick={() => UserStore.setAdminPopup(false)}>Ok</Button>
                                    </div>
                                </div>
                            ) : null
                        }
                        {UserStore.adminKindOfPopup == "deletePost"
                            ? (
                                <div className='popup_inner'>
                                    <h3 className="text-center mb-4">{UserStore.adminPoupMessages}</h3>
                                    <div className="w-100 m-auto delete-selection">
                                        <Button theme="primary" size="lg" className="delete-yes" onClick={() => this.deletePost()}>Có</Button>
                                        <Button size="lg" className="delete-no" onClick={() => UserStore.setAdminPopup(false)}>Không</Button>
                                    </div>
                                </div>
                            ) : null
                        }
                    </div>
                ) : null
            }
        </div>
      )
    }
}

export default AdminPopup;