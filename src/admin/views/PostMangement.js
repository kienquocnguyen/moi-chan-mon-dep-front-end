import React from "react";
import { Container, Row, Col, Card, CardBody, CardTitle, Button } from "shards-react";
import ImageUploader from "react-images-upload";
import UserStore from "../../MobxStore/UserStore";

import PageTitle from "../components/common/PageTitle";
import UpdateEditor from "../components/post-management/UpdateEditor";
import { observer } from "mobx-react";
import PostTable from "../components/post-management/PostTable";


@observer
class PostMangement extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      editingImage: false
    }
  }

  componentDidMount = async () =>{
    
  }

  onDropPostavatar = picture => {
    UserStore.setPostAvatar(picture);
    console.log(UserStore.postAvatar);
  };
  editImage = () =>{
    this.setState({editingImage: !this.state.editingImage})
  }
  render() {
    return (
      <Container fluid className="main-content-container px-4 pb-4">
        {/* Page Header */}
        <Row noGutters className="page-header py-4">
          <PageTitle sm="4" title="Bài Viết" subtitle="Trang Quản Lý Bài Viết" className="text-sm-left" />
        </Row>

        <Row>
          {/* Editor */}
          <Col lg="12" md="12">
          </Col>
          <Col lg="12" md="12">
            <PostTable/>
            <Card className="mb-3">
              <CardBody>
                <ImageUploader
                  withIcon={true}
                  buttonText="Choose images"
                  onChange={this.onDropPostavatar}
                  imgExtension={[".jpg", ".gif", ".png", ".gif"]}
                  singleImage={true}
                  withPreview={true}
                />
                <Button theme="primary" className="mt-2 w-100" onClick={this.editImage}>Chỉnh Sửa</Button>
              </CardBody>
            </Card>
            {UserStore.singlePost
            ? (
              <UpdateEditor
                singlePost = {UserStore.singlePost}
              />
            ) : null
            }
          </Col>
        </Row>
      </Container>
    )
  }
}

export default PostMangement;
