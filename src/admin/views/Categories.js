import React from "react";
import { Container, Row, Col, Card, CardBody, Form, FormSelect, FormInput, Button, CardTitle, ListGroupItem, InputGroup, FormTextarea } from "shards-react";
import ImageUploader from "react-images-upload";
import UserStore from "../../MobxStore/UserStore";
import CheckBoxCategories from "../components/add-new-post/CheckBoxCategories";
import PageTitle from "../components/common/PageTitle";
import { observer } from "mobx-react";
import AdminPopup from "../components/AdminPopup";

@observer
class Categories extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
        selectedValue: ""
    }
  }
  componentDidMount = async () =>{
    await UserStore.getCategories();
  }
  selectedParent = (e, childId) =>{
    this.setState({selectedValue: e.target.value})
    let selectedValue = e.target.value;
    let separateValue = selectedValue.match(/([^,]+)/g)
    UserStore.setSelectedParentCategory(childId, separateValue[1])
  }
  detleteCategories = async (category) =>{
    UserStore.setDeleteCategoryId(category._id)
    UserStore.setAdminPopupMessages("Bạn có chắc là sẽ xóa danh mục này không ?")
    UserStore.setAdminPopup(true, "deleteCategoreis")
  }
  updateCategoriesParent = async () =>{
    await UserStore.updateCategoryParent();
    await UserStore.getCategories();
  }

  handleCategoryName = (e) =>{
    this.setState({title: e.target.value});
  }
  handleCategoryDescription = (e) =>{
    this.setState({description: e.target.value});
  }
  addNewCategories = async () =>{
    const categories = new FormData();
    categories.append("title", this.state.title);
    categories.append("description", this.state.description);
    categories.append("avatar", UserStore.categoriesImage[0]);
    await UserStore.createCategory(categories);
    await UserStore.getCategories();
  }
  onDropCategorytavatar = picture => {
    UserStore.setCategoriesImage(picture);
  };

  render() {
    return (
      <Container fluid className="main-content-container adnin-categories px-4 pb-4">
        {UserStore.showAdminPopup
          ? (
            <AdminPopup/>
          ) : null
        }
        {/* Page Header */}
        <Row noGutters className="page-header py-4">
          <PageTitle sm="4" title="Danh Mục" subtitle="Trang Quản Lý Danh Mục Bài Viết" className="text-sm-left" />
        </Row>

        <Card small className="mb-3 new-categories">
            <CardTitle className="p-4">Tạo Danh Mục Mới</CardTitle>
            <CardBody>
                <ListGroupItem className="d-block px-3">
                    <InputGroup className="ml-auto mt-2">
                        <FormInput placeholder="Tên Danh Mục Mới" onChange={(e) => this.handleCategoryName(e)}/>
                    </InputGroup>
                    <FormTextarea placeholder="Mô Tả Danh Mục Mới" className="mt-2" onChange={(e) => this.handleCategoryDescription(e)}/>
                </ListGroupItem>
                <ListGroupItem className="d-flex px-3">
                    <ImageUploader
                        withIcon={true}
                        buttonText="Choose images"
                        onChange={this.onDropCategorytavatar}
                        imgExtension={[".jpg", ".gif", ".png", ".gif"]}
                        singleImage={true}
                        withPreview={true}
                    />
                </ListGroupItem>
                <ListGroupItem className="d-block px-3">
                    <Button theme="primary" className="mt-2 w-100" onClick={this.addNewCategories}>
                        Thêm Danh Mục Bài Viết
                    </Button>
                </ListGroupItem>
            </CardBody>
        </Card>
        <Card small className="mb-3 categories-management">
            <CardTitle className="p-4">Danh Sách Danh Mục</CardTitle>
            {UserStore.categoriesList
            ? (
                <CardBody>
                    {UserStore.categoriesList.map((list, idx) => (
                        <div>
                            <Row>
                                <Col lg="9" md="6">
                                    <CheckBoxCategories
                                        name={list.title}
                                        checked={true}
                                        class="ml-1 mt-3"
                                        key={idx}
                                    />
                                </Col>
                                <Col lg="3" md="6" className="edit-categories">
                                    <FormSelect className="mr-3" onChange={(e) => this.selectedParent(e, list._id)}>
                                        <option>Chưa có danh mục cha</option>
                                        {UserStore.categoriesList.map((categoriesPartent, idx) => (
                                            <option>{categoriesPartent.title}, {categoriesPartent._id}</option>
                                        ))}
                                    </FormSelect>
                                    <button className="categories-delete" onClick={() => this.detleteCategories(list)}>Xóa</button>
                                    <Button theme="primary" size="md" onClick={() => this.updateCategoriesParent(list)}>Cập Nhật</Button>
                                </Col>
                            </Row>
                            {list.categoriesChildren
                                ? (
                                    <div>
                                        {list.categoriesChildren.map((child, childIdx) => (
                                            <div>
                                                <Row>
                                                    <Col lg="9" md="6">
                                                        <CheckBoxCategories
                                                            name={child.title}
                                                            checked={true}
                                                            class="ml-4 mt-3"
                                                            key={childIdx}
                                                        />
                                                    </Col>
                                                    <Col lg="3" md="6" className="edit-categories">
                                                        <FormSelect className="mr-3" onChange={(e) => this.selectedParent(e, child._id)}>
                                                            <option>{list.title} (Đang Chọn)</option>
                                                            {UserStore.categoriesList.map((categoriesPartent, idx) => (
                                                                <option>{categoriesPartent.title}, {categoriesPartent._id}</option>
                                                            ))}
                                                        </FormSelect>
                                                        <button class="categories-delete" onClick={() => this.detleteCategories(child)}>Xóa</button>
                                                        <Button theme="primary" size="md" onClick={() => this.updateCategoriesParent(child)}>Cập Nhật</Button>
                                                    </Col>
                                                </Row>
                                            {child.categoriesChildren
                                                ? (
                                                    <div>
                                                        {child.categoriesChildren.map((subChild, subChildIdx) => (
                                                            <Row>
                                                                <Col lg="9" md="6">
                                                                    <CheckBoxCategories
                                                                        name={subChild.title}
                                                                        checked={true}
                                                                        class="ml-5 mt-3"
                                                                        key={subChildIdx}
                                                                    />
                                                                </Col>
                                                                <Col lg="3" md="6" className="edit-categories">
                                                                    <FormSelect className="mr-3" onChange={(e) => this.selectedParent(e, subChild._id)}>
                                                                        <option>{child.title} (Đang Chọn)</option>
                                                                        {UserStore.categoriesList.map((categoriesPartent, idx) => (
                                                                            <option>{categoriesPartent.title}, {categoriesPartent._id}</option>
                                                                        ))}
                                                                    </FormSelect>
                                                                    <button class="categories-delete" onClick={() => this.detleteCategories(subChild)}>Xóa</button>
                                                                    <Button theme="primary" size="md" onClick={() => this.updateCategoriesParent(subChild)}>Cập Nhật</Button>
                                                                </Col>
                                                            </Row>
                                                        ))}
                                                    </div>
                                                ):null
                                            }
                                            </div>
                                        ))}
                                    </div>
                                ) : null
                            }
                        </div>
                    ))}
                </CardBody>
                ) : (
                   <CardBody className="pb-5">
                       <h1 className="text-center text-danger">Bạn Chưa Tạo Danh Mục Nào</h1>
                   </CardBody>
                )
            }
        </Card>
      </Container>
    )
  }
}

export default Categories;
